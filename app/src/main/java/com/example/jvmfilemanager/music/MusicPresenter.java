package com.example.jvmfilemanager.music;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import androidx.fragment.app.FragmentManager;

import com.example.jvmfilemanager.base.BasePresenter;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.example.jvmfilemanager.favorite.FavoriteContract;
import com.example.jvmfilemanager.images.Images;
import com.example.jvmfilemanager.videos.VideoModel;
import com.google.android.exoplayer2.source.MediaSource;

import java.util.ArrayList;
import java.util.List;

public class MusicPresenter<V extends MusicContract.View> extends BasePresenter<V> implements MusicContract.Presenter<V>, LoadDBListener {

    @Override
    public void loadAllMusic(Context context) {
        List<MusicModel> musicModelist=new ArrayList<>();




            Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, (String) null, (String[]) null, null);
            if (cursor != null) {
                try {

                    while (cursor.moveToNext()) {
                        String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                        String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                        String filepath = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                        String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));

                        MusicModel musicModel=new MusicModel();
                        musicModel.setAlbum(album);
                        musicModel.setName(title);
                        musicModel.setPath(filepath);
                        musicModelist.add(musicModel);
                    }

                    cursor.close();
                } catch (Exception e) {
                }

                getView().setMusicList(musicModelist);

        }
    }

//        return musicModelist;

    // added: 20/10/22
    private FragmentManager fragmentManager;

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }
    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public void onGarbageDel() {

    }


}
