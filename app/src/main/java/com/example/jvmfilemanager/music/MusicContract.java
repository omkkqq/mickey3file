package com.example.jvmfilemanager.music;

import android.content.Context;
import android.net.Uri;

import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;

import java.util.List;

public interface MusicContract {
     interface View extends BaseContract{
        void setMusicList(List<MusicModel> musicList);
        void setPlayer(Uri uri);
    }

    interface Adapter{
        void  setDataList(List<MusicModel> musicList);
    }

    interface Presenter<V extends View> extends BaseAttachImpl<V>{
        void loadAllMusic(Context c);
    }
}
