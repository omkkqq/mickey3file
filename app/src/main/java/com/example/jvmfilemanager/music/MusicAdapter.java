package com.example.jvmfilemanager.music;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jvmfilemanager.LongClickDialogFragment;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MusicAdapter   extends RecyclerView.Adapter<MusicAdapter.ViewHolder> implements  MusicContract.Adapter, LoadDBListener {
    private Context context;
    List<MusicModel> list=new ArrayList<>();
    private SimpleExoPlayer player;

    private MusicPresenter<MusicContract.View> presenter;
    public MusicAdapter(MusicPresenter<MusicContract.View> presenter) {
        this.presenter = presenter;
    }



    @NonNull
    @Override
    public MusicAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View itemView=inflater.inflate(R.layout.item_all,parent,false);
        context=parent.getContext();
        return new ViewHolder(itemView,this);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicAdapter.ViewHolder holder, int position) {
        holder.bind(list.get(position));
        list.get(position).setPosition(position);
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public void onGarbageDel() {

    }

    @Override
    public void setDataList(List<MusicModel> musicList) {
        this.list=musicList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        LoadDBListener loadDBListener;
        TextView fileName;

        ImageView imageView;
        Uri musicUri;

        View bottomSheet;
        public ViewHolder(@NonNull View itemView,LoadDBListener listener) {
            super(itemView);
            this.loadDBListener=listener;
            fileName=itemView.findViewById(R.id.gabName);
            loadDBListener=listener;
            imageView=itemView.findViewById(R.id.gabImg);

        }

        private void bind(final MusicModel fileModel){
            fileName.setText(fileModel.getName());
            Log.d("musicpath", "bind: "+fileModel.getPath());

            MediaScannerConnection.scanFile(context, new String[] {new File(fileModel.getPath()).getAbsolutePath() }, null,
                    (path, uri) -> musicUri=uri );

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        presenter.getView().setPlayer(musicUri);
//                        initializePlayer(musicUri);


                }
            });


            // btn long click listener, added: 20/10/22
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AllFileModel allFileModel=new AllFileModel();
                    allFileModel.setFileName(fileModel.getName());
                    allFileModel.setFilePath(fileModel.getPath());
                    BottomSheetDialogFragment bottomSheet = new LongClickDialogFragment()
                            .setName(fileModel.getName(),context)
                            .setFavoriteFileNameAndPath(allFileModel,loadDBListener);
                    bottomSheet.show(presenter.getFragmentManager(), bottomSheet.getTag());
                    return false;
                }
            });
        }
//        public void initializePlayer(Uri uri) {
//            //String source = "file:///storage/sdcard0/Sounds/Music/song.mp3";
//            Log.d("uri", "initializePlayer: "+uri.getPath());
//
//
//            String source=uri.toString();
//            if (player != null) {
//                Log.d("uri", "release: ");
//                release();
//            }
//                player =
//                        ExoPlayerFactory.newSimpleInstance(
//                                new DefaultRenderersFactory(context), new DefaultTrackSelector(), new
//                                        DefaultLoadControl());
//                dataSourceFactory =
//                        new DefaultDataSourceFactory(
//                                context, Util.getUserAgent(context, "Music Player"), null);
//                extractorsFactory = new DefaultExtractorsFactory();
//                // The MediaSource represents the media to be played.
//                MediaSource mediaSource =
//                        new ExtractorMediaSource(
//                                Uri.parse(source), dataSourceFactory, extractorsFactory, null, null);
//                player.prepare(mediaSource, true, true);
//                player.setPlayWhenReady(true);
//
//
//
//        }
//
//        private void release() {
//            player.setPlayWhenReady(false);
//            player.stop();
//            player.seekTo(0);
//            extractorsFactory =null;
//            dataSourceFactory = null;
//            mediaSource = null;
//        }
//    }
  }
}
