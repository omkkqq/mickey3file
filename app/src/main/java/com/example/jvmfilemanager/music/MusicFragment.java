package com.example.jvmfilemanager.music;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jvmfilemanager.EndListener;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.BaseFragment;
import com.example.jvmfilemanager.videos.VideoAdapter;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.List;


public class MusicFragment extends BaseFragment implements EndListener ,MusicContract.View{

    private RecyclerView recyclerView;
    private MusicPresenter<MusicContract.View> presenter=new MusicPresenter<>();
    private Context context;
    private  BottomSheetBehavior behavior;
    private MusicAdapter adapter;
    ExtractorsFactory extractorsFactory;
    DataSource.Factory dataSourceFactory;
    MediaSource mediaSource;
    private SimpleExoPlayer player;
    SimpleExoPlayerView exoPlayerView;
    private Uri oldUri;
    private Long currentPosition=0L;
    View bottomSheet;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setFragmentManager(getFragmentManager());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = container.getContext();
        View v=inflater.inflate(R.layout.fragment_music, container, false);
        exoPlayerView=v.findViewById(R.id.exoplayerview);
        recyclerView=v.findViewById(R.id.musicRecycle);
         bottomSheet=v.findViewById(R.id.design_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);


        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.onAttached(this);
        presenter.loadAllMusic(context);

        BottomSheetBehavior.from(bottomSheet).addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState==BottomSheetBehavior.STATE_HIDDEN ){
                    Log.d("1104", "onStateChanged: "+newState);
                    release();
                    player.setPlayWhenReady(true);
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                Log.d("1104", "onSlide: "+slideOffset);
            }
        });
    }

    @Override
    public void onEndPicture() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void init() {

    }

    @Override
    public void setMusicList(List<MusicModel> musicList) {
        adapter=new MusicAdapter(presenter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setDataList(musicList);
    }

    @Override
    public void setPlayer(Uri uri) {
        initializePlayer(uri);
    }
    private boolean slide=false;
    private boolean isStop=false;
    public void initializePlayer(Uri uri) {
        //String source = "file:///storage/sdcard0/Sounds/Music/song.mp3";

        slide=false;
        Log.d("uri", "initializePlayer: "+uri.getPath());
        showBottomSheet(behavior);

        String source=uri.toString();
        if (player != null) {
            Log.d("uri", "release: ");
            currentPosition = player.getCurrentPosition();
            Log.d("currentPosition", "initializePlayer: "+currentPosition);
            release();
        }


        player =
                ExoPlayerFactory.newSimpleInstance(
                        new DefaultRenderersFactory(context), new DefaultTrackSelector(), new
                                DefaultLoadControl());

        if (oldUri != null){
            if (uri.equals(oldUri)) {
                isStop=!isStop;
                if (isStop){
                    player.setPlayWhenReady(false);
                }
                else {
                    player.setPlayWhenReady(true);
                }
                player.seekTo(currentPosition);
            }
            else {
                player.setPlayWhenReady(true);
            }
        }
        else {
            player.setPlayWhenReady(true);
        }

        oldUri=uri;


        exoPlayerView.setPlayer(player);
        dataSourceFactory =
                new DefaultDataSourceFactory(
                        context, Util.getUserAgent(context, "Music Player"), null);
        extractorsFactory = new DefaultExtractorsFactory();
        // The MediaSource represents the media to be played.
        MediaSource mediaSource =
                new ExtractorMediaSource(
                        Uri.parse(source), dataSourceFactory, extractorsFactory, null, null);
        player.prepare(mediaSource, true, true);






    }
    private void showBottomSheet(BottomSheetBehavior behavior) {

       // if (behavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

//        } else {
//            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//
//        }
    }

    private void  hiddenBottomSheet(BottomSheetBehavior behavior){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
    private void release() {
        player.setPlayWhenReady(false);
        player.stop();
        player.seekTo(0);
        extractorsFactory =null;
        dataSourceFactory = null;
        mediaSource = null;
    }

}
