package com.example.jvmfilemanager.favorite;

import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;
import com.example.jvmfilemanager.db.FavoriteEntity;

import java.util.LinkedHashMap;
import java.util.List;

public interface FavoriteContract  {
    interface View extends BaseContract{
        void onGetFavoriteAllData(List<AllFileModel> fileModels);
        void getDataFromPath(List<AllFileModel> list,String nowPath);
        void updateLinkedMap(LinkedHashMap<String,List<AllFileModel>> linkedHashMap);
    }

    interface Adapter {
        void setDataList(List<AllFileModel> list);
    }

    interface Presenter<V extends  View> extends BaseAttachImpl<V>{
        void getFavoriteAllData();
        void deleteFavoriteData(FavoriteEntity favoriteEntity);
        void getNowPathMoreData(String path);
    }
}
