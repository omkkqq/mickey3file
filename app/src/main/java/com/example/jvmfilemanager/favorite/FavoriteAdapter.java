package com.example.jvmfilemanager.favorite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jvmfilemanager.LongClickDialogFragment;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> implements FavoriteContract.Adapter, LoadDBListener  {
    private LinkedList<String> nowInThis=new LinkedList<>();
    private List<AllFileModel> dataList=new ArrayList<>();
    FavoritePresenter<FavoriteContract.View> presenter ;
    private LoadDBListener loadDBListener;
    private Context context;
    public void setPresenter(FavoritePresenter<FavoriteContract.View> presenter){
        this.presenter=presenter;
    }

    @Override
    public void setDataList(List<AllFileModel> list) {
        dataList.clear();
        dataList=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        context=parent.getContext();
        View itemView=layoutInflater.inflate(R.layout.item_favorite_layout,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        loadDBListener=this;
        holder.bind(dataList.get(position));
        dataList.get(position).setPosition(position);
    }

    @Override
    public int getItemCount() {
        return dataList==null?0:dataList.size();
    }

    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {
        presenter.getView().showToast(error);
    }

    @Override
    public void onComplete(String status) {
        if (status.equals(presenter.getView().getResourceString(R.string.db_update))){
            presenter.getView().showToast(R.string.db_add_ok);
        }
        else if (status.equals(presenter.getView().getResourceString(R.string.db_update))){
            presenter.getView().showToast(R.string.db_update_ok);
        }

    }

    @Override
    public void onDeleteComplete() {
        presenter.getView().showToast(R.string.db_delete_ok);
    }

    @Override
    public void onGarbageDel() {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView fileName;
        ImageView imageView;
        boolean isFavorite=true;
        ImageView favoriteBtn;
        ConstraintLayout favoriteConstarint;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
//            favoriteConstarint=itemView.findViewById(R.id.favoriteConstarint);
            fileName=itemView.findViewById(R.id.favoriteText);
            imageView=itemView.findViewById(R.id.favoriteImg);
//            favoriteBtn=itemView.findViewById(R.id.favoriteLove);
        }

        private void bind(final AllFileModel allFileModel){
            fileName.setText(allFileModel.getFileName());
            imageView.setImageResource(setFileImg(allFileModel));

//            favoriteBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    isFavorite=!isFavorite;
//                    if (isFavorite){
//                        favoriteBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
//                    }
//                    else {
//                        favoriteBtn.setImageResource(R.drawable.ic_favorite_border_black_24dp);
//                        FavoriteEntity favoriteEntity=new FavoriteEntity();
//                        favoriteEntity.id=allFileModel.getId();
//                        favoriteEntity.fileExtension=allFileModel.getExtension();
//                        favoriteEntity.fileName=allFileModel.getFileName();
//                        favoriteEntity.filePath=allFileModel.getFilePath();
//                        presenter.deleteFavoriteData(favoriteEntity);
//                    }
//
//                }
//            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.getNowPathMoreData(allFileModel.getFilePath());
                    nowInThis.add(allFileModel.getFilePath());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    BottomSheetDialogFragment bottomSheet = new LongClickDialogFragment()
                            .setName(allFileModel.getFileName(),context)
                            .setFavoriteFileNameAndPath(allFileModel,loadDBListener);

                    bottomSheet.show(presenter.getFragmentManager(), bottomSheet.getTag());
                    return false;
                }
            });
        }


    }
    private int setFileImg(AllFileModel allFileModel){
//            #FF6F00
        if (allFileModel.getExtension().equals("directory")){
            return R.drawable.ic_folder;
        }
        else if (allFileModel.getExtension().equals("pdf")){
            return R.drawable.ic_picture_as_pdf_black_24dp;
        }
        else if (allFileModel.getExtension().equals("zip") || allFileModel.getExtension().equals("7z")){
            return R.drawable.ic_fzip;
        }
        else if (allFileModel.getExtension().equals("tar")){
            return R.drawable.ic_tar_file_format_symbol;
        }
        else if (allFileModel.getExtension().equals("csv")){
            return R.drawable.ic_csv_file_format_extension;
        }
        else if (allFileModel.getExtension().equals("bmp")){
            return R.drawable.ic_bmp_file_format_symbol;
        }
        else if (allFileModel.getExtension().equals("apk")){
            return R.drawable.ic_android_black_24dp;
        }
        else if (allFileModel.getExtension().equals("jpg") || allFileModel.getExtension().equals("png")){
            return R.drawable.ic_picture;
        }
        else if (allFileModel.getExtension().equals("mp3") || allFileModel.getExtension().equals("mp4")
                || allFileModel.getExtension().equals("wav")){
            return R.drawable.ic_music;
        }
        else {
            return R.drawable.ic_file;
        }
    }

    public LinkedList<String> getNowInThis(){
        return nowInThis;
    }
}
