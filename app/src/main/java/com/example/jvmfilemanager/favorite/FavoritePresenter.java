package com.example.jvmfilemanager.favorite;

import android.util.Log;

import androidx.fragment.app.FragmentManager;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BasePresenter;
import com.example.jvmfilemanager.db.DataRepository;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FavoritePresenter<V extends FavoriteContract.View> extends BasePresenter<V> implements FavoriteContract.Presenter<V>, LoadDBListener {

    List<AllFileModel> fileModelList;
    DataRepository repository=new DataRepository();

    @Override
    public void getFavoriteAllData() {
        repository.loadItems(this);
    }

    @Override
    public void deleteFavoriteData(FavoriteEntity favoriteEntity) {
        repository.deleteItems(this,favoriteEntity);

    }


    @Override
    public void getNowPathMoreData(String path) {

        getView().showProgressDialog(R.string.loading);
        File file=new File(path);
//        File[]   files = new File("/storage/emulated/0").listFiles();
        File[] files=file.listFiles();
        List<AllFileModel> fileList=new ArrayList<>();
        try {
            if (files!=null && files.length>0){
                for (File nowFile : files){
                    AllFileModel allFileModel=new AllFileModel();
                    allFileModel.setFilePath(nowFile.getCanonicalPath());
                    allFileModel.setFileName(nowFile.getName());
                    allFileModel.setExtension(getFileExtension(nowFile));
                    if (nowFile.isDirectory()) allFileModel.setDirectly(true);
                    fileList.add(allFileModel);
                }
                Log.d("22222", "getNowPathMoreData: "+fileList);
                getView().dismissProgressDialog();
                getView().getDataFromPath(fileList,path);
            }else if ( fileList.size()==0){
                getView().showToast(R.string.fileNullOrfilelistNull);
                getView().dismissProgressDialog();
            }
        } catch (IOException e) {
            e.printStackTrace();
            getView().showToast(R.string.IOEexception);
            getView().dismissProgressDialog();
        }


    }
    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "directory"; // empty extension
        }
        return name.substring(lastIndexOf+1);
    }

    /**
        *  dataBase CallBack
        * */
    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {
        fileModelList=new ArrayList<>();
        for (FavoriteEntity entity:favoriteEntityList){
            AllFileModel allFileModel=new AllFileModel();
            allFileModel.setFileName(entity.fileName);
            allFileModel.setFilePath(entity.filePath);
            allFileModel.setExtension(entity.fileExtension);
            allFileModel.setId(entity.id);
            fileModelList.add(allFileModel);
        }
        getView().onGetFavoriteAllData(fileModelList);
    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {
        getView().showToast(error);
    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {
        repository.loadItems(this);
    }

    @Override
    public void onGarbageDel() {

    }

    private FragmentManager fragmentManager;

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

}
