package com.example.jvmfilemanager.favorite;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jvmfilemanager.EndListener;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.all.AllFragmentAdapter;
import com.example.jvmfilemanager.all.BtnPlusAdapter;
import com.example.jvmfilemanager.base.BaseFragment;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

/**
*
 */
public class FavoriteFragment extends BaseFragment implements FavoriteContract.View , EndListener {

    private RecyclerView recyclerView;
    FavoritePresenter<FavoriteContract.View> presenter=new FavoritePresenter<>();
    private FavoriteAdapter adapter=new FavoriteAdapter();
    private Context context;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context=view.getContext();
        recyclerView=view.findViewById(R.id.favoriteRecycleView);
        presenter.onAttached(this);
        presenter.setFragmentManager(getFragmentManager());
        presenter.getFavoriteAllData();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getFavoriteAllData();
    }

    @Override
    public void onGetFavoriteAllData(List<AllFileModel> fileModels) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
        adapter.setPresenter(presenter);
        adapter.setDataList(fileModels);
    }

    @Override
    public void getDataFromPath(List<AllFileModel> list, String nowPath) {
        adapter.setDataList(list);
    }

    @Override
    public void updateLinkedMap(LinkedHashMap<String, List<AllFileModel>> linkedHashMap) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void init() {

    }


    @Override
    public void onEndPicture() {

    }

    @Override
    public boolean onBackPressed() {
        LinkedList<String> linkedList;
        linkedList=adapter.getNowInThis();
        if (linkedList.size()>1){
            linkedList.removeLast();
            presenter.getNowPathMoreData(linkedList.getLast());
            return true;
        }
        else {
            presenter.getFavoriteAllData();
            return true;
        }

    }
}
