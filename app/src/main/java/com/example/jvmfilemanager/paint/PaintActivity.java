package com.example.jvmfilemanager.paint;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.BaseActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PaintActivity extends BaseActivity {
    Boolean isMode=false;
    private Activity context;
    final static String IS_BG="IS_BG";
    String ffPath="";
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);
        final PPView paintView = findViewById(R.id.paintView);
        final Button eraser  =findViewById(R.id.eraser);
        Button back  =findViewById(R.id.back);
        final Button go  =findViewById(R.id.go);
        Button save  =findViewById(R.id.save);
        Button next  =findViewById(R.id.next);
        final SeekBar prgress_width =findViewById(R.id.widthprogress);
        final SeekBar eraser_width =findViewById(R.id.eraserprogress);
        final SeekBar transparentbar =findViewById(R.id.transparenctbar);
        final Button colorRed =findViewById(R.id.red);
        final Button colorYellow =findViewById(R.id.yellow);
        final View chColorLayout =findViewById(R.id.chColorLayout);
        final View orange =findViewById(R.id.orange);
        final View green =findViewById(R.id.green);
        final View blue =findViewById(R.id.blue);
        final View black =findViewById(R.id.black);
        ImageView horse =findViewById(R.id.horse);
        context=this;

        Intent intent = getIntent();
        bundle =intent.getExtras().getBundle("undle");
        if (bundle.getByteArray("EXTRA_BTYEARRAY")!=null && (bundle.getInt(IS_BG)==1)){
            byte[] b = bundle.getByteArray("EXTRA_BTYEARRAY");
            Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);
            BitmapDrawable ob = new BitmapDrawable(getResources(), bmp);
            horse.setImageDrawable(ob);
            String FILE_PATH="FILE_PATH";
            ffPath=bundle.getString(FILE_PATH);
        }else if (bundle.getInt(IS_BG)==0){
             String FILE_PATH="FILE_PATH";
             ffPath=bundle.getString(FILE_PATH);
        }


        final Drawable drawable = horse.getDrawable();

        if(drawable != null){
            isMode=true;
        }else{
            isMode=false;
        }






        eraser.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewSaveToImage(paintView,ffPath);

            }
        });

        final int trans=255;
        int percent=0;
        go.setOnClickListener(new View.OnClickListener() {
            boolean clickit=false;
            int cl= Color.BLACK;
            int a=10;
            @Override
            public void onClick(View v) {
                eraser_width.setVisibility(View.GONE);
                clickit=!clickit;
                if (clickit){
                    eraser.setEnabled(false);
                    eraser.setBackgroundResource(R.drawable.eraser);
                    go.setBackgroundResource(R.drawable.ic_paintbrush_sl);
                    transparentbar.setVisibility(View.VISIBLE);
                    prgress_width.setVisibility(View.VISIBLE);
                    chColorLayout.setVisibility(View.VISIBLE);
                }
                else{
                    eraser.setEnabled(true);
                    eraser.setBackgroundResource(R.drawable.eraser);
                    go.setBackgroundResource(R.drawable.ic_paintbrush);
                    transparentbar.setVisibility(View.GONE);
                    prgress_width.setVisibility(View.GONE);
                    chColorLayout.setVisibility(View.GONE);
                }


                paintView.setColor(cl,trans);
                paintView.setWidth(a);
                prgress_width.setOnSeekBarChangeListener(
                   new SeekBar.OnSeekBarChangeListener() {
                     @Override
                     public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                         paintView.setWidth(seekBar.getProgress());
                     }

                     @Override
                     public void onStartTrackingTouch(SeekBar seekBar) {

                     }

                     @Override
                     public void onStopTrackingTouch(SeekBar seekBar) {

                     }
                 });


                transparentbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        int percent=seekBar.getProgress();
                        int trans=(256/100)*percent;
                        paintView.setColor(cl,trans);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                colorRed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl= Color.RED;
                        paintView.setColor(cl,trans) ;
                    }
                });



                colorYellow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl= Color.YELLOW;
                        paintView.setColor(cl,trans);
                    }
                });

                black.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl= Color.BLACK;
                        paintView.setColor(cl,trans);
                    }
                });


                orange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl= Color.MAGENTA;
                        paintView.setColor(cl,trans);
                    }
                });
                green.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl= Color.GREEN;
                        paintView.setColor(cl,trans);
                    }
                });

                blue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl= Color.BLUE;
                        paintView.setColor(cl,trans);
                    }
                });
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               paintView.back();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paintView.next();
            }
        });

        eraser.setOnClickListener(new View.OnClickListener() {
            boolean clickNext=false;
            int progress=0;

            @Override
            public void onClick(View v) {

                clickNext=!clickNext;

                if (clickNext){
                    eraser.setBackgroundResource(R.drawable.eraser_slected);
                    go.setBackgroundResource(R.drawable.ic_paintbrush);
                    go.setEnabled(false);
                    prgress_width.setVisibility(View.GONE);
                    chColorLayout.setVisibility(View.GONE);
                    transparentbar.setVisibility(View.GONE);
                    eraser_width.setVisibility(View.VISIBLE);
                    paintView.setWidth(progress);
                    eraser_width.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            progress=seekBar.getProgress();
                            paintView.setWidth(progress);
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });

                }else{
                    eraser.setBackgroundResource(R.drawable.eraser);
                    go.setBackgroundResource(R.drawable.ic_paintbrush);
                    go.setEnabled(true);
                    prgress_width.setVisibility(View.GONE);
                    eraser_width.setVisibility(View.GONE);
                }
                paintView.eraserIt(isMode);

            }
        });



    }
    String fileName="";
    private void viewSaveToImage(final PPView view, String filePath) {





            //語法一：new AlertDialog.Builder(主程式類別).XXX.XXX.XXX;
            if (bundle.getInt(IS_BG)==1){
                String[] bits = filePath.split("/");
                final String lastOne = bits[bits.length-1];
                String ll=filePath.substring(0,filePath.indexOf(lastOne));
                Log.d("viewSaveToImage", "viewSaveToImage: "+ll);
                final File appDir = new File(ll);
                LayoutInflater inflater = LayoutInflater.from(this);
                final View v = inflater.inflate(R.layout.alertdialog_use, null);
                new AlertDialog.Builder(this)
                        .setTitle("輸入新檔案名稱或是用原本的名字"+lastOne)
                        .setView(v)
                        .setPositiveButton(R.string.check, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FileOutputStream fos;
                                Bitmap cachebmp = view.loadBitmapFromView();
                                EditText editText = v.findViewById(R.id.editText1);
                                fileName = editText.getText().toString();
                                Log.d("fileName", "veToImage: " + fileName);
                                Toast.makeText(PaintActivity.this, "新檔案名稱" +
                                        editText.getText().toString(), Toast.LENGTH_SHORT).show();
                                if (fileName.isEmpty()) {
                                    fileName = "test";
                                }
                                if (!appDir.exists()) {
                                    appDir.mkdir();
                                }
                                File file = new File(appDir, lastOne);
                                try {
                                    fos = new FileOutputStream(file);
                                    cachebmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
                                    fos.flush();
                                    fos.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                context.finish();

                            }
                        })
                        .show();
            }else {
                final File appDir = new File(filePath);
                LayoutInflater inflater = LayoutInflater.from(this);
                final View v = inflater.inflate(R.layout.alertdialog_use, null);
                new AlertDialog.Builder(this)
                        .setTitle("輸入新檔案名稱或是用預設名稱(test.png)")
                        .setView(v)
                        .setPositiveButton(R.string.check, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FileOutputStream fos;
                                Bitmap cachebmp = view.loadBitmapFromView();
                                EditText editText = v.findViewById(R.id.editText1);
                                fileName = editText.getText().toString();
                                Log.d("fileName", "veToImage: " + fileName);
                                Toast.makeText(PaintActivity.this, "新檔案名稱" +
                                        editText.getText().toString(), Toast.LENGTH_SHORT).show();
                                if (fileName.isEmpty()) {
                                    fileName = "test";
                                }
                                if (!appDir.exists()) {
                                    appDir.mkdir();
                                }
                                File file = new File(appDir, fileName + ".png");
                                try {
                                    fos = new FileOutputStream(file);
                                    cachebmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
                                    fos.flush();
                                    fos.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                context.finish();

                            }
                        })
                        .show();
            }

            Log.d("fileName", "veToImage: " + fileName);


            view.destroyDrawingCache();

    }
}
