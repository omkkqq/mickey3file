package com.example.jvmfilemanager.images;

import android.content.Context;

import androidx.paging.DataSource;

class MyDataSourceFactory extends androidx.paging.DataSource.Factory {

    Context context;
    public MyDataSourceFactory(Context context) {
        this.context=context;
    }

    @Override
    public DataSource<Integer,Images> create() {
        return new MyDataSource(context);
    }
}
