package com.example.jvmfilemanager.images;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.example.jvmfilemanager.all.AllFragmentContract;
import com.example.jvmfilemanager.base.BasePresenter;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ImagesPresenter <V extends  ImageContract.View> extends BasePresenter<V> implements ImageContract.Presenter<V> {

    @Override
    public void queryImgs(Context context) {
        List<Images> list = new ArrayList<>();


        String[] projection = new String[0];
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            projection = new String[]{MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.RELATIVE_PATH,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.SIZE,
                    MediaStore.Images.Media.MIME_TYPE,
                    MediaStore.Images.Media.WIDTH,
                    MediaStore.Images.Media.HEIGHT,
                    MediaStore.Images.Media.DATE_MODIFIED};
            String sortOrder = "date_modified DESC";
            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, (String) null, (String[]) null, sortOrder);
            if (cursor != null) {
                try {

                    while (cursor.moveToNext()) {
                        long id = cursor.getLong(cursor.getColumnIndex("_id"));
                        String path = cursor.getString(cursor.getColumnIndex("relative_path"));
                        String name = cursor.getString(cursor.getColumnIndex("_display_name"));
                        String size = cursor.getString(cursor.getColumnIndex("_size"));
                        String width = cursor.getString(cursor.getColumnIndex("width"));
                        String height = cursor.getString(cursor.getColumnIndex("height"));
                        String date = cursor.getString(cursor.getColumnIndex("date_modified"));
                        Uri uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
                        if (size != null) {
                            Images var21 = new Images(id, uri, path, name, size, width, height, date);
                            list.add(var21);
                        }
                    }

                    cursor.close();
                } catch (Exception e) {
                }
            }
        }
       else {
            File imgFile;
            projection = new String[]{MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.SIZE,
                    MediaStore.Images.Media.MIME_TYPE,
                    MediaStore.Images.Media.WIDTH,
                    MediaStore.Images.Media.HEIGHT,
                    MediaStore.Images.Media.DATE_MODIFIED};
            String sortOrder = "date_modified DESC";

            String orderBy = MediaStore.Images.Media.DATE_MODIFIED ;

            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, (String) null, (String[]) null, sortOrder);
            if (cursor != null) {
                try {

                    while (cursor.moveToNext()) {
                        int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                        long id = cursor.getLong(cursor.getColumnIndex("_id"));
                        imgFile = new File(cursor.getString(dataColumnIndex));
                        Uri uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
                        Images var21 = new Images(id, uri, imgFile.getAbsolutePath(), imgFile.getName(), "", "", "", String.valueOf(imgFile.lastModified()));
                        list.add(var21);
                    }

                    cursor.close();
                } catch (Exception e) {
                }
            }
        }
        Log.d("imgggggggggggga", "queryImgs: "+list.size());
        getView().setImagesData(list);

    }

    //230311
    private FragmentManager fragmentManager;

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

}
