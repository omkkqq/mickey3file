package com.example.jvmfilemanager.images;

import android.content.Context;

import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.all.AllFragmentContract;
import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

public interface ImageContract {
    interface View extends BaseContract {
        void setImagesData(List<Images> list);
        void openIntent(File f);
    }

    interface Adapter{

    }


    interface  Presenter<V extends ImageContract.View> extends BaseAttachImpl<V> {

        void queryImgs(Context context);
    }
}
