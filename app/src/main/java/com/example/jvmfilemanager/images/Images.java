package com.example.jvmfilemanager.images;

import android.net.Uri;

public class Images {
    public Images(Long id, Uri uri, String path, String name, String size, String width, String height, String date) {
        this.id = id;
        this.uri = uri;
        this.path = path;
        this.name = name;
        this.size = size;
        this.width = width;
        this.height = height;
        this.date = date;
    }

    public Images() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    private Long id=0L;
    private Uri uri;
    private String path="";
    private String name="";
    private String size="";
    private String width="";
    private String  height="";
    private String date="";

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private int position=0;
}
