package com.example.jvmfilemanager.images;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jvmfilemanager.EndListener;
import com.example.jvmfilemanager.LongClickDialogFragment;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.example.jvmfilemanager.images.imagebig.ImgBigActivity;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;


class ImagesAdapter extends PagedListAdapter<Images, ImagesAdapter.ViewHolder> implements LoadDBListener {
    private boolean isLoaded=false;
    private List<String> pathList=new ArrayList<>();
    private List<Integer> positionList=new ArrayList<>();
    private Context context;
    private ImagesPresenter<ImageContract.View> presenter;
    private int position=0;
    public ImagesAdapter() {
        super(mDiffCallback);

    }
    public void setPathList(List<String> azhList,boolean ll){
        pathList=new ArrayList<>();
        pathList=azhList;
        isLoaded=true;
        Log.d("imgggggggggggggggg", "setPathList: "+pathList.size()+"ssss"+isLoaded);
    }

    public void  setPresenter(ImagesPresenter<ImageContract.View> presenter){
        this.presenter=presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View itemView=inflater.inflate(R.layout.item_image_v,parent,false);
        context=parent.getContext();
        return new ViewHolder(itemView,this);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Images data = getItem(position);
        data.setPosition(position);
        Log.d("imgggggggggggggggg", "isLoaded: "+isLoaded);
        if (!isLoaded){
            Log.d("imgggggggggggggggg", "isLoaded: ");
            pathList.add(data.getUri().toString());
        }

        try {
            holder.bind(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private static final  DiffUtil.ItemCallback<Images> mDiffCallback = new DiffUtil.ItemCallback<Images>() {
        @Override
        public boolean areItemsTheSame(@NonNull Images oldItem,@NonNull Images newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull Images oldItem,@NonNull Images newItem) {
            return (oldItem == newItem);
        }
    };

    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public void onGarbageDel() {

    }


    public class ViewHolder  extends  RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textView;
        LoadDBListener loadDBListener;
        private ViewHolder(@NonNull View itemView,LoadDBListener listener) {
            super(itemView);
            imageView=itemView.findViewById(R.id.imageView);
            textView=itemView.findViewById(R.id.imgName);
            loadDBListener=listener;
        }

        private void bind( Images images) throws IOException {
            textView.setText(images.getName());
//            Glide.with(context)
//                    .load(new File(images.getUri().getPath()))
//                    .into(imageView);// Uri of the picture
            Log.d("imgggggggggggggggg", "aa: "+pathList.size());
            Glide.with(context).load(images.getUri()).into(imageView);

            imageView.setOnClickListener(
                    v -> {
                        Intent intent=new Intent(context, ImgBigActivity.class);
                        intent.putStringArrayListExtra("pathList", (ArrayList<String>) pathList);
                        intent.putExtra("position",images.getPosition());
                        context.startActivity(intent);
                    }
            );
            itemView.setOnLongClickListener(view -> {
                AllFileModel allFileModel=new AllFileModel();
                allFileModel.setFilePath(images.getPath());
                allFileModel.setFileName(images.getName());
                allFileModel.setExtension("圖片");
                allFileModel.setFileGBMB(formetFileSizeToGBMB(Long.valueOf(images.getSize())));
                allFileModel.setLastModifyDate(Long.parseLong(images.getDate()));
                BottomSheetDialogFragment bottomSheet = new LongClickDialogFragment()
                        .setName(images.getName(),context)
                        .setFavoriteFileNameAndPath(allFileModel,loadDBListener);
                bottomSheet.show(presenter.getFragmentManager(), bottomSheet.getTag());
                return false;
            });
          //  imageView.setImageDrawable();
        }
        //轉換檔案大小為GB/MB
        public String formetFileSizeToGBMB(long fileS) {//轉換檔案大小
            DecimalFormat df = new DecimalFormat("#.00");
            String fileSizeString = "";
            if (fileS < 1024) {
                fileSizeString = df.format((double) fileS) + "B";
            } else if (fileS < 1048576) {
                fileSizeString = df.format((double) fileS / 1024) + "K";
            } else if (fileS < 1073741824) {
                fileSizeString = df.format((double) fileS / 1048576) + "M";
            } else {
                fileSizeString = df.format((double) fileS / 1073741824) + "G";
            }
            return fileSizeString;
        }
    }
}
