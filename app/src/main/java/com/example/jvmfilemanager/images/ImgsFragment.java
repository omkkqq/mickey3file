package com.example.jvmfilemanager.images;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.jvmfilemanager.EndListener;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.BaseFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ImgsFragment extends BaseFragment implements EndListener,ImageContract.View {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private  ImagesPresenter<ImageContract.View> presenter=new ImagesPresenter<>();;
    private Context context;
    private ImagesAdapter mAdapter=new ImagesAdapter();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        presenter.setFragmentManager(getFragmentManager());
        context=container.getContext();
        return inflater.inflate(R.layout.fragment_imgs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        presenter.onAttached(this);
        presenter.queryImgs(context);
        RecyclerView mRecyclerView=view.findViewById(R.id.imgRecycleView);
      //  presenter.queryImgs(context);
        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(10)
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(10)
                .build();


        LiveData<PagedList<Images>> liveData =
                new LivePagedListBuilder(new MyDataSourceFactory(context), config)
                .build();
        liveData.observe(this, dataBeans -> mAdapter.submitList(dataBeans));
        GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setPresenter(presenter);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onEndPicture() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void init() {

    }

    @Override
    public void setImagesData(List<Images> list) {

        presenter=new ImagesPresenter<>();
        List<String> pathList=new ArrayList<>();
        for (Images image:list){
            pathList.add(image.getUri().toString());
        }

        mAdapter.setPathList(pathList,true);
    }

    @Override
    public void openIntent(File f) {

    }


    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "directory"; // empty extension
        }
        return name.substring(lastIndexOf+1);
    }
}
