package com.example.jvmfilemanager.images.imagebig;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.ImageView;

import com.example.jvmfilemanager.R;

import java.util.ArrayList;

public class ImgBigActivity extends AppCompatActivity {

    private FullScreenImageAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_big);
        ViewPager pager=findViewById(R.id.pager);
        ImageView imageView=findViewById(R.id.id_back_arrow);
        imageView.setOnClickListener(v ->  finish());
        ArrayList<String> test = getIntent().getStringArrayListExtra("pathList");
        int position=getIntent().getIntExtra("position",0);
        adapter=new FullScreenImageAdapter(this,test,position);
        pager.setAdapter(adapter);
        pager.setCurrentItem(position);

    }
}
