package com.example.jvmfilemanager.images.imagebig;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.images.Images;
import com.ortiz.touchview.TouchImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class FullScreenImageAdapter extends PagerAdapter {
    private Activity _activity;
    private ArrayList<String> mediaFileListModelArrayList;
    private int position=0;
    private Context context;
    // constructor
    public FullScreenImageAdapter(Activity activity, ArrayList<String> mediaFileListModelArrayList,int position) {
        this._activity = activity;
        this.mediaFileListModelArrayList=mediaFileListModelArrayList;
        this.position=position;
    }

    @Override
    public int getCount() {
        return this.mediaFileListModelArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        context=container.getContext();
        LayoutInflater inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,false);
        TouchImageView imgDisplay =  viewLayout.findViewById(R.id.imgDisplay);
        String images=mediaFileListModelArrayList.get(position);
        Log.d("imgggggggggggggggg", "instantiateItem: "+Uri.parse(images)+"pppppp"+position);
      //  Glide.with(container.getContext()).load(Uri.parse(images)).into(imgDisplay);
        try {
            imgDisplay.setImageBitmap(getThumbnail(Uri.parse(images)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ((ViewPager) container).addView(viewLayout);
        return viewLayout;
    }
    public  Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException{
        InputStream input = context.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither=true;//optional
        onlyBoundsOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;
;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//
        input = context.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}