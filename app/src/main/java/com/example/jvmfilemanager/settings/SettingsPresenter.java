package com.example.jvmfilemanager.settings;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BaseContract;
import com.example.jvmfilemanager.base.BasePresenter;

import java.util.LinkedHashMap;
import java.util.List;

public class SettingsPresenter <V extends SettingsContract.View> extends BasePresenter<V> implements SettingsContract.Presenter<V>{

    private final String _email= "s1410832010@gms.nutc.edu.tw";

    /**
     * 建立 intent 給 activity 開啟
     * */
    @Override
    public void sendEmail() {
        try {
            // 不能 getResource()
            Uri uri = Uri.fromParts("mailto", _email, null)
                    .buildUpon()
                    .build();

            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
            getView().sendEmail(emailIntent);
        } catch (Exception e) {
            e.printStackTrace();
            getView().showToast(e.hashCode());
        }
    }


    public void onSpinnerThemeClick(int position) {
        Log.d("themeSpinnerClick", String.valueOf(position));
        switch (position) {
            case 0:
                Log.d("settings spinner", "onThemeSelected: 0");
                break;
            case 1:
                Log.d("settings spinner", "onThemeSelected: 1");
                break;
            case 2:
                Log.d("settings spinner", "onThemeSelected: 2");
                break;
        }
    }
    public void onSpinnerColorClick(int position) {
        switch (position) {
            case 0:
                Log.d("settings spinner", "onColorSelected: 0");
                break;
            case 1:
                Log.d("settings spinner", "onColorSelected: 1");
                break;
            case 2:
                Log.d("settings spinner", "onColorSelected: 2");
                break;
        }
    }

}
