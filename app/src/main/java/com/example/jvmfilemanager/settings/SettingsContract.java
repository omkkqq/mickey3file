package com.example.jvmfilemanager.settings;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.all.AllFragmentContract;
import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;

import java.util.LinkedHashMap;
import java.util.List;

public interface SettingsContract {

    interface View extends BaseContract {
        void sendEmail(Intent intent);
    }

    interface Presenter<V extends SettingsContract.View> extends BaseAttachImpl<V> {
        void sendEmail();
    }

    interface Adapter{

    }
}
