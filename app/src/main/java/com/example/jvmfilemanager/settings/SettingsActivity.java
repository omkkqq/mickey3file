package com.example.jvmfilemanager.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.BaseActivity;
import com.example.jvmfilemanager.base.SettingPreference;
import com.example.jvmfilemanager.home.HomeActivity;


public class SettingsActivity extends BaseActivity implements SettingsContract.View, View.OnClickListener{

    private SettingsPresenter<SettingsContract.View> presenter=new SettingsPresenter<>();

    private ConstraintLayout layoutTheme;
    private ConstraintLayout layoutColor;
    private ConstraintLayout layoutFontSize;
    private ConstraintLayout layoutFeedback;

    private ConstraintLayout layoutThemeOption;
    private ConstraintLayout layoutColorOption;
    private ConstraintLayout layoutFontSizeOption;

    private SettingPreference settingPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        presenter.onAttached(this);
        final Context context=this;
        Button button=findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

        layoutTheme= findViewById(R.id.layoutSettingsTheme);
        layoutColor= findViewById(R.id.layoutSettingsColor);
        layoutFontSize = findViewById(R.id.layoutSettingsFontSize);
                settingPreference=new SettingPreference(context);
                settingPreference.setFontSize(1F);
                try {
                    adjustFontScale(context.getResources().getConfiguration(),settingPreference.getFontSize(),context);
                    Intent intent = new Intent(SettingsActivity.this, SettingsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Settings.SettingNotFoundException e) {
                    e.printStackTrace();
                }
                Intent intent=new Intent(SettingsActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
        // feedback layout
        layoutFeedback= findViewById(R.id.layoutSettingsFeedback);
//        layoutTheme.setOnClickListener(this);
//        layoutColor.setOnClickListener(this);
//        layoutFontSize.setOnClickListener(this);
//        layoutFeedback.setOnClickListener(this);
    }


    /**
     * 從參數拿到的 intent 開啟 activity
     * @param intent ACTION_SEND.
     * */
    @Override
    public void sendEmail(Intent intent) {
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.feedback)));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layoutSettingsFeedback:
                presenter.sendEmail();
                break;
        }
    }
}
