package com.example.jvmfilemanager.garbage;

import android.content.Context;

import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.images.ImageContract;
import com.example.jvmfilemanager.images.Images;

import java.io.File;
import java.util.List;

public interface GarbageContract {
    interface View extends BaseContract {
        void setImagesDataList(List<GarbageEntity> list);
        void openIntent(File f);
    }

    interface Adapter{
        void setDataList(List<GarbageEntity> list);
    }


    interface  Presenter<V extends GarbageContract.View> extends BaseAttachImpl<V> {
        void listAllGarbage();
    }
}
