package com.example.jvmfilemanager.garbage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jvmfilemanager.GarbageListener;
import com.example.jvmfilemanager.LongClickDialogFragment;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.db.DataRepository;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.example.jvmfilemanager.images.Images;
import com.example.jvmfilemanager.images.imagebig.ImgBigActivity;
import com.example.jvmfilemanager.music.MusicAdapter;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class GarbageAdapter  extends RecyclerView.Adapter<GarbageAdapter.ViewHolder> implements  GarbageContract.Adapter ,LoadDBListener{
    GarbagePresenter<GarbageContract.View> presenter=new GarbagePresenter<>();
    List<GarbageEntity> list=new ArrayList<>();
    private Context context;
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View itemView=inflater.inflate(R.layout.item_garbage,parent,false);
        context=parent.getContext();
        return new ViewHolder(itemView,this);
    }

    public void setPresenter(GarbagePresenter<GarbageContract.View> presenter){
        this.presenter=presenter;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.bind(list.get(position));
        } catch (IOException e) {
            e.printStackTrace();
        }
        list.get(position).setPosition(position);
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }


    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public void onGarbageDel() {

    }

    @Override
    public void setDataList(List<GarbageEntity> list) {
        this.list=list;
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        LoadDBListener listener;
        ImageView imageView;
        TextView textView;
        ImageView reduction;
        public ViewHolder(@NonNull View itemView,LoadDBListener listener) {
            super(itemView);
            this.listener=listener;
            imageView=itemView.findViewById(R.id.gabImg);
            textView=itemView.findViewById(R.id.gabName);
            reduction=itemView.findViewById(R.id.gabRedu);
        }

        private void bind(GarbageEntity images) throws IOException {
            textView.setText(images.getFileName());
            reduction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("ppppppppppp", "onClick: ");

                        moveFile(images.afterFilePath,images.beforeFilePath,images.fileName,images.fileExtension);

                }
            });
            itemView.setOnLongClickListener(view -> {

                AllFileModel allFileModel=new AllFileModel();
                allFileModel.setFilePath(images.getAfterFilePath());
                allFileModel.setFileName(images.getFileName());
                allFileModel.setExtension(images.getFileExtension());


                BottomSheetDialogFragment bottomSheet = new LongClickDialogFragment()
                        .setName(images.fileName,context)
                        .setFavoriteFileNameAndPath(allFileModel,listener);

                bottomSheet.show(presenter.getFragmentManager(), bottomSheet.getTag());
                return false;
            });
            //  imageView.setImageDrawable();
        }
    }

    private void moveFile(String inputPath,String outputPath,String filename, String fileExtension)  {
        Log.d("aaaaaaaa", "inputPath: "+inputPath+File.separator+filename+"\n outputPath"+outputPath+"\n    filename"+filename);
        try {

            File file=new File(inputPath+File.separator+filename);
            file.renameTo(new File(outputPath));
            DataRepository repository=new DataRepository();
            repository.deletGarbageItem(this,filename);
        }
        catch (Exception e){
            Log.d("aaaaaaaaa", "moveFile: "+e);
        }


    }

}
