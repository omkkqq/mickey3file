package com.example.jvmfilemanager.garbage;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jvmfilemanager.EndListener;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.BaseFragment;
import com.example.jvmfilemanager.db.DataRepository;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;


import java.io.File;
import java.util.List;


public class GarbageFragment extends BaseFragment implements GarbageContract.View, EndListener  {

    private GarbageAdapter adapter;
    private RecyclerView recyclerView;
    private GarbagePresenter<GarbageContract.View> presenter=new GarbagePresenter<>() ;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setFragmentManager(getFragmentManager());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_garbage, container, false);
        context=container.getContext();
        recyclerView=v.findViewById(R.id.garbageRecycleView);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onAttached(this);
        presenter.listAllGarbage();


    }

    @Override
    public void onEndPicture() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void setImagesDataList(List<GarbageEntity> list) {
        adapter=new GarbageAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
        adapter.setDataList(list);
        adapter.setPresenter(presenter);
    }

    @Override
    public void openIntent(File f) {

    }

    @Override
    public void init() {

    }



}
