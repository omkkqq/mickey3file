package com.example.jvmfilemanager.garbage;

import android.content.Context;

import androidx.fragment.app.FragmentManager;

import com.example.jvmfilemanager.all.AllFragmentContract;
import com.example.jvmfilemanager.base.BasePresenter;
import com.example.jvmfilemanager.db.DataRepository;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;

import java.util.List;

public class GarbagePresenter <V extends  GarbageContract.View> extends BasePresenter<V> implements GarbageContract.Presenter<V>, LoadDBListener {
    @Override
    public void listAllGarbage() {
        DataRepository repository=new DataRepository();
        repository.loadGarbageItem(this);

    }

    private FragmentManager fragmentManager;

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {
        getView().setImagesDataList(garbageEntityList);
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public void onGarbageDel() {
        listAllGarbage();
    }
}
