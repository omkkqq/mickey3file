package com.example.jvmfilemanager.videos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFragmentAdapter;
import com.example.jvmfilemanager.base.BaseFragment;

import java.util.List;


public class VideosFragment extends BaseFragment implements  VideoContract.View {
    private Context context;
    private VideosPresenter<VideoContract.View> presenter=new VideosPresenter<>();
    private VideoAdapter adapter;
    private RecyclerView recyclerView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setFragmentManager(getFragmentManager());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=container.getContext();
        View v=inflater.inflate(R.layout.fragment_videos, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recyclerView=view.findViewById(R.id.videoRecycleView);
        presenter.onAttached(this);
        presenter.getAllVideos(context);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onEndPicture() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void getVideoList(List<VideoModel> list) {
        adapter=new VideoAdapter(presenter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setDataList(list);
    }

    @Override
    public void init() {

    }
}
