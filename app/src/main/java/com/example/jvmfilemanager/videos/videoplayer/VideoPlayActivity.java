package com.example.jvmfilemanager.videos.videoplayer;


import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.BaseActivity;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;

public class VideoPlayActivity extends BaseActivity {

    SimpleExoPlayerView exoPlayerView;
    SimpleExoPlayer player;
    Uri videoUri;
    View  nextButton,previousButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);
        init();

    }


    public void init(){
        Bundle bundle = this.getIntent().getExtras().getBundle("videoBundle");
       String videoPath=bundle.getString("videoUri");
        nextButton = findViewById(R.id.exo_next);
        previousButton=findViewById(R.id.exo_prev);
//        Uri  videoUri= Uri.fromFile(new File(videoPath));
        MediaScannerConnection.scanFile(this, new String[] {new File(videoPath).getAbsolutePath() }, null,
                (path, uri) -> videoUri=uri );
        videoUri=Uri.fromFile(new File(videoPath));
        Log.d("aaaaaaaaaaaaa", "init: "+videoUri);
        exoPlayerView=findViewById(R.id.exoplayerview);
        initializePlayer(videoUri);

    }

    public void initializePlayer(Uri uri){


        if (player==null){

            player= ExoPlayerFactory.newSimpleInstance(
                    new DefaultRenderersFactory(this),
                    new DefaultTrackSelector(),
                    new DefaultLoadControl());
            exoPlayerView.setPlayer(player);
            ComponentListener componentListener=new ComponentListener();
            player.addListener(componentListener);
            player.setPlayWhenReady(true);
//            player.seekTo(currentWindow,playbackPosition);

            MediaSource mediaSource=buildMediaSource(uri);
            player.prepare(mediaSource,true,false);
        }
    }

    private MediaSource buildMediaSource(Uri uri){
        return new ExtractorMediaSource(uri,
                new DefaultDataSourceFactory(this,"ua"),
                new DefaultExtractorsFactory(),null,null);

    }

    public  class ComponentListener implements ExoPlayer.EventListener, TimeBar.OnScrubListener,
            View.OnClickListener {



        @Override
        public void onScrubStart(TimeBar timeBar) {

        }

        @Override
        public void onScrubMove(TimeBar timeBar, long position) {
//            if (positionView != null) {
//                positionView.setText(Util.getStringForTime(formatBuilder, formatter, position));
//            }
        }

        @Override
        public void onScrubStop(TimeBar timeBar, long position, boolean canceled) {
//            scrubbing = false;
//            if (!canceled && player != null) {
//                seekToTimebarPosition(position);
//            }
//            hideAfterTimeout();
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//            updatePlayPauseButton();
//            updateProgress();
        }

        @Override
        public void onPositionDiscontinuity() {
//            updateNavigation();
//            updateProgress();
        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            // Do nothing.
        }

        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest) {
//            updateNavigation();
//            updateTimeBarMode();
//            updateProgress();
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
            // Do nothing.
        }

        @Override
        public void onTracksChanged(TrackGroupArray tracks, TrackSelectionArray selections) {
            // Do nothing.
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            // Do nothing.
        }

        @Override
        public void onClick(View view) {
            if (player != null) {
                if (nextButton == view) {
                    player.stop();
                    player.seekTo(0L);
                    MediaSource mediaSource=buildMediaSource(videoUri);
                    player.prepare(mediaSource);
                }
                } else if (previousButton == view) {
                Log.d("151115", "onClick: ");
                player.stop();
                player.seekTo(0L);
                MediaSource mediaSource=buildMediaSource(videoUri);
                player.prepare(mediaSource);

//                } else if (fastForwardButton == view) {
//                    fastForward();
//                } else if (rewindButton == view) {
//                    rewind();
//                } else if (playButton == view) {
//                    controlDispatcher.dispatchSetPlayWhenReady(player, true);
//                } else if (pauseButton == view) {
//                    controlDispatcher.dispatchSetPlayWhenReady(player, false);
//                }
            }
//            hideAfterTimeout();
        }

    }

}
