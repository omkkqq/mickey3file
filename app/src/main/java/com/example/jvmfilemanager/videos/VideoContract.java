package com.example.jvmfilemanager.videos;

import android.content.Context;
import android.net.Uri;

import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;
import com.example.jvmfilemanager.sdcard.SdCardContract;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

public interface VideoContract {
    interface View extends BaseContract {
        void getVideoList(List<VideoModel> list);


    }

    interface Adapter {
        void setDataList(List<VideoModel> list);
    }

    interface Presenter<V extends VideoContract.View> extends BaseAttachImpl<V> {
        void getAllVideos(Context context);
    }
}
