package com.example.jvmfilemanager.videos;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jvmfilemanager.LongClickDialogFragment;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.example.jvmfilemanager.videos.videoplayer.VideoPlayActivity;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class VideoAdapter  extends RecyclerView.Adapter<VideoAdapter.ViewHolder> implements  VideoContract.Adapter, LoadDBListener {

    private VideosPresenter<VideoContract.View> presenter;

    private Context context;
    List<VideoModel> list=new ArrayList<>();
    public VideoAdapter(VideosPresenter<VideoContract.View> presenter) {
        this.presenter = presenter;
    }
    @NonNull
    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View itemView=inflater.inflate(R.layout.item_all,parent,false);
        context=parent.getContext();
        return new ViewHolder(itemView,this);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoAdapter.ViewHolder holder, int position) {
        holder.bind(list.get(position));
        list.get(position).setPosition(position);
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public void onGarbageDel() {

    }

    @Override
    public void setDataList(List<VideoModel> list) {
        this.list=list;
        notifyDataSetChanged();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LoadDBListener loadDBListener;
        TextView fileName;
        TextView filePath;
        ImageView imageView;
        public ViewHolder(@NonNull View itemView,LoadDBListener listener) {
            super(itemView);

            fileName=itemView.findViewById(R.id.gabName);
            loadDBListener=listener;
            imageView=itemView.findViewById(R.id.gabImg);
        }

        private void bind(final VideoModel fileModel){
            fileName.setText(fileModel.getVideoTitle());
            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(new File(fileModel.getVideoUri().getPath()).getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
            imageView.setImageBitmap(
                  bMap
                   );
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, VideoPlayActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("videoUri",fileModel.getVideoUri().toString());
                    intent.putExtra("videoBundle",bundle);
                    context.startActivity(intent);
                }
            });

            AllFileModel allFileModel=new AllFileModel();
            allFileModel.setFileName(fileModel.getVideoTitle());
            // btn long click listener, added: 20/10/22
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    BottomSheetDialogFragment bottomSheet = new LongClickDialogFragment()
                            .setName(fileModel.getVideoTitle(),context)
                            .setFavoriteFileNameAndPath(allFileModel,loadDBListener);
                    bottomSheet.show(presenter.getFragmentManager(), bottomSheet.getTag());
                    return false;
                }
            });
        }
    }


}
