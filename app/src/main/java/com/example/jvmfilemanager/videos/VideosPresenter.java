package com.example.jvmfilemanager.videos;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.fragment.app.FragmentManager;

import com.example.jvmfilemanager.base.BasePresenter;
import com.example.jvmfilemanager.sdcard.SdCardContract;

import java.util.ArrayList;
import java.util.List;

public class VideosPresenter<V extends VideoContract.View> extends BasePresenter<V> implements VideoContract.Presenter<V> {

    @Override
    public void getAllVideos(Context context){
        List<VideoModel> videoModels=new ArrayList<>();
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        Cursor cursor = contentResolver.query(uri, null, null, null, null);

        //looping through all rows and adding to list
        if (cursor != null && cursor.moveToFirst()) {
            do {

                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.TITLE));
                String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
                VideoModel  videoModel  = new VideoModel ();
                videoModel .setVideoTitle(title);
                videoModel .setVideoUri(Uri.parse(data));
                videoModel.setVideoDuration(duration);

//                videoModel .setVideoDuration(timeConversion(Long.parseLong(duration)));
                videoModels.add(videoModel);

            } while (cursor.moveToNext());
        }
        getView().getVideoList(videoModels);

    }

    // added: 20/10/22
    private FragmentManager fragmentManager;

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }
}
