package com.example.jvmfilemanager.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {GarbageEntity.class}, version = 1, exportSchema = false)
public abstract class GarbageDataBase  extends RoomDatabase {
    private static  GarbageDataBase instance;

    public abstract GarbageDao catGarbageDao();

    public static GarbageDataBase getGarbageDataBase(Context context){
        if (instance==null) {
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    GarbageDataBase.class,
                    "garbage-db")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

}
