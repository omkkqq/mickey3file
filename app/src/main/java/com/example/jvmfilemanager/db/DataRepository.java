package com.example.jvmfilemanager.db;

import android.util.Log;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.App;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class DataRepository implements  DataInteractor {

    CompositeDisposable mDisposable = new CompositeDisposable();


    public CompositeDisposable getmDisposable() {
        return mDisposable;
    }


    @Override
    public void insertGarbagesItem(LoadDBListener listener, GarbageEntity entity) {
        GarbageDataBase database = GarbageDataBase.getGarbageDataBase(App.getAppContext());
        mDisposable.add(database.catGarbageDao().insert(entity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action() {
                    @Override
                    public void run() throws Exception {
                        listener.onComplete(App.getAppContext().getString(R.string.db_add));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("aaa", "accept: "+throwable);
                        listener.onError("Error"+throwable);
                    }
                }));
    }

    @Override
    public void loadGarbageItem(LoadDBListener listener) {
        GarbageDataBase database = GarbageDataBase.getGarbageDataBase(App.getAppContext());
        mDisposable.add(database.catGarbageDao().loadGarbageEntity()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<GarbageEntity>>() {
                               @Override
                               public void accept(List<GarbageEntity> garbageEntities) throws Exception {
                                   Log.d("aaasssssssssa",""+garbageEntities.size());
                                   listener.onGarbageFinished(garbageEntities);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Log.d("errrrrrrrrr",throwable+"ee");
                                listener.onError("Error"+throwable);
                            }
                        }));

    }

    @Override
    public void deletGarbageItem(LoadDBListener listener,String  filename) {
        GarbageDataBase database = GarbageDataBase.getGarbageDataBase(App.getAppContext());

        mDisposable.add(database.catGarbageDao().deleteByFileName(filename)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action() {
                    @Override
                    public void run() throws Exception {
                        Log.d("rrrr", "run: ");
                        listener.onDeleteComplete();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        listener.onError("Error"+throwable);
                    }
                }));
    }

    @Override
    public void findLikeData(final LoadDBListener listener, String searchData) {
        AppDataBase database = AppDataBase.getAppDataBase(App.getAppContext());
        mDisposable.add(database.catFavoriteDao().findFavoriteFile(searchData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<FavoriteEntity>>() {
                               @Override
                               public void accept(List<FavoriteEntity> favoriteEntityList) throws Exception {
                                   listener.onFavoriteFinished(favoriteEntityList);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Log.d("errrrrrrrrr",throwable+"ee");
                                listener.onError("Error"+throwable);
                            }
                        }));
    }

    @Override
    public void loadItems(final LoadDBListener listener) {
        AppDataBase database = AppDataBase.getAppDataBase(App.getAppContext());
        mDisposable.add(database.catFavoriteDao().loadAllFavoriteList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<List<FavoriteEntity>>() {
                           @Override
                           public void accept(List<FavoriteEntity> favoriteEntityList) throws Exception {
                                listener.onFavoriteFinished(favoriteEntityList);
                           }
                       },
                    new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("errrrrrrrrr",throwable+"ee");
                            listener.onError("Error"+throwable);
                        }
                    }));

    }

    @Override
    public void updateItems(final LoadDBListener listener, FavoriteEntity favoriteEntity) {
        AppDataBase database = AppDataBase.getAppDataBase(App.getAppContext());
        mDisposable.add(database.catFavoriteDao().update(favoriteEntity)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Action() {
            @Override
            public void run() throws Exception {
                listener.onComplete(App.getAppContext().getString(R.string.db_update));
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                listener.onError("Error"+throwable);
            }
        }));
    }

    @Override
    public void onStop() {
        mDisposable.clear();
    }

    @Override
    public void addItems(final LoadDBListener listener, FavoriteEntity entity) {
        AppDataBase database = AppDataBase.getAppDataBase(App.getAppContext());
        mDisposable.add(database.catFavoriteDao().insert(entity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action() {
                    @Override
                    public void run() throws Exception {
                        listener.onComplete(App.getAppContext().getString(R.string.db_add));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        listener.onError("Error"+throwable);
                    }
                }));
    }

    @Override
    public void deleteItems(final LoadDBListener listener, FavoriteEntity entity) {
        AppDataBase database = AppDataBase.getAppDataBase(App.getAppContext());

        mDisposable.add(database.catFavoriteDao().delete(entity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action() {
                    @Override
                    public void run() throws Exception {
                        listener.onDeleteComplete();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        listener.onError("Error"+throwable);
                    }
                }));
    }

    @Override
    public String findDataRepeatOrNot(FavoriteEntity entity) {
        AppDataBase database = AppDataBase.getAppDataBase(App.getAppContext());
        return database.catFavoriteDao().findPathIsOrNotReapeat(entity.filePath);
    }

    @Override
    public void deleteByFilePath(final LoadDBListener listener, FavoriteEntity entity) {
        AppDataBase database = AppDataBase.getAppDataBase(App.getAppContext());

        mDisposable.add(database.catFavoriteDao().deleteByFilePath(entity.filePath)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action() {
                    @Override
                    public void run() throws Exception {
                        listener.onDeleteComplete();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        listener.onError("Error"+throwable);
                    }
                }));
    }




}
