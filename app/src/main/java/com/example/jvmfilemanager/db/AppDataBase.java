package com.example.jvmfilemanager.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {FavoriteEntity.class}, version = 1, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase {

    private static  AppDataBase instance;

    public abstract FavoriteDao catFavoriteDao();

    public static AppDataBase getAppDataBase(Context context){
        if (instance==null) {
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDataBase.class,
                    "favorite-db")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }


}
