package com.example.jvmfilemanager.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface GarbageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(GarbageEntity... garbageEntities);

    @Query("SELECT * FROM GarbageEntity")
    Flowable<List<GarbageEntity>>  loadGarbageEntity();

    @Query("DELETE FROM GarbageEntity WHERE file_name = :file_name")
    Completable deleteByFileName(String file_name);

}
