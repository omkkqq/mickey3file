package com.example.jvmfilemanager.db;

import java.util.List;

public interface LoadDBListener {

    void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList);
    void onGarbageFinished(List<GarbageEntity> garbageEntityList);
    void onError(String error);
    void onComplete(String status);
    void onDeleteComplete();
    void onGarbageDel();
}
