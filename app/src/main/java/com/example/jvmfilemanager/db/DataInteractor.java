package com.example.jvmfilemanager.db;

import java.util.List;

public interface DataInteractor {

    void findLikeData(LoadDBListener listener,String searchData);

    void loadItems(LoadDBListener listener);

    void updateItems(LoadDBListener listener, FavoriteEntity favoriteEntity);

    void onStop();

    void addItems(LoadDBListener listener,FavoriteEntity entity);

    void deleteItems(LoadDBListener listener,FavoriteEntity entity);

    String findDataRepeatOrNot(FavoriteEntity entity);

    void deleteByFilePath(LoadDBListener listener,FavoriteEntity entity);

    void insertGarbagesItem(LoadDBListener listener,GarbageEntity entity);

    void loadGarbageItem(LoadDBListener listener);

    void deletGarbageItem(LoadDBListener listener,String filename);

}
