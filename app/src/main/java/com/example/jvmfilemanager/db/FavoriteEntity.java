package com.example.jvmfilemanager.db;

import androidx.annotation.StringRes;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class FavoriteEntity {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "file_name")
    public String fileName;

    @ColumnInfo(name = "file_path")
    public String filePath;

    @ColumnInfo(name = "file_extension" )
    public String fileExtension;


}
