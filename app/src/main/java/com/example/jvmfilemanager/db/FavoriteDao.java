package com.example.jvmfilemanager.db;

import androidx.annotation.StringRes;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface FavoriteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(FavoriteEntity... favoriteEntities);

    @Insert
    void insert(List<FavoriteEntity> favoriteEntityList);

    @Update
    Completable update(FavoriteEntity... favoriteEntities);

    @Delete
    Completable delete(FavoriteEntity... favoriteEntities);

    @Query("SELECT * FROM FavoriteEntity WHERE file_name LIKE '%' || :searchFile || '%' ")
    Flowable<List<FavoriteEntity>>  findFavoriteFile(String searchFile);

    @Query("SELECT * FROM FavoriteEntity WHERE file_path LIKE '%' || :searchFilePath || '%' ")
    Flowable<List<FavoriteEntity>>  findFavoriteFilePath(String searchFilePath);

    @Query("SELECT * FROM FavoriteEntity")
    Flowable<List<FavoriteEntity>>  loadAllFavoriteList();

    @Query("SELECT file_path  FROM FavoriteEntity WHERE file_path = :file_path LIMIT 1")
    String findPathIsOrNotReapeat(String file_path);

    @Query("DELETE FROM FavoriteEntity WHERE file_path = :file_path")
    Completable deleteByFilePath(String file_path);

}
