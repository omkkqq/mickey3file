package com.example.jvmfilemanager.search;

import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;

import java.util.List;

public interface SearchContract {
    interface View extends BaseContract{
        void getSearchData(List<AllFileModel> list);
    }

    interface Presenter<V extends View>extends BaseAttachImpl<V> {
        void searchData(String data,String whereIs);
        void searchSQLData(String data);
    }

    interface Adapter {
        void setDataList(List<AllFileModel> list);
    }
}
