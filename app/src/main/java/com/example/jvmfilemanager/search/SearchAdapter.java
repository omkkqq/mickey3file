package com.example.jvmfilemanager.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;


import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> implements SearchContract.Adapter{
    List<AllFileModel> list = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View itemView=inflater.inflate(R.layout.item_search,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        list.get(position).setPosition(position);
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    @Override
    public void setDataList(List<AllFileModel> list) {
        this.list=list;
        notifyDataSetChanged();
    }

    class ViewHolder extends  RecyclerView.ViewHolder {
        TextView fileName;
        ImageView searchImg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fileName=itemView.findViewById(R.id.search_text);
            searchImg=itemView.findViewById(R.id.searchImg);


        }

        private void bind(final AllFileModel fileModel){

            if (fileModel!=null){
                fileName.setText(fileModel.getFileName());
                searchImg.setImageResource(setFileImg(fileModel
                ));
            }


            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                }
            });
        }
        private int setFileImg(AllFileModel allFileModel){
//            #FF6F00
            if (allFileModel.getExtension().equals("directory")){
                return R.drawable.ic_folder;
            }
            else if (allFileModel.getExtension().equals("pdf")){
                return R.drawable.ic_picture_as_pdf_black_24dp;
            }
            else if (allFileModel.getExtension().equals("zip") || allFileModel.getExtension().equals("7z")){
                return R.drawable.ic_fzip;
            }
            else if (allFileModel.getExtension().equals("tar")){
                return R.drawable.ic_tar_file_format_symbol;
            }
            else if (allFileModel.getExtension().equals("csv")){
                return R.drawable.ic_csv_file_format_extension;
            }
            else if (allFileModel.getExtension().equals("bmp")){
                return R.drawable.ic_bmp_file_format_symbol;
            }
            else if (allFileModel.getExtension().equals("apk")){
                return R.drawable.ic_android_black_24dp;
            }
            else if (allFileModel.getExtension().equals("jpg") || allFileModel.getExtension().equals("png")){
                return R.drawable.ic_picture;
            }
            else if (allFileModel.getExtension().equals("mp3") || allFileModel.getExtension().equals("mp4")
                    || allFileModel.getExtension().equals("wav")){
                return R.drawable.ic_music;
            }
            else {
                return R.drawable.ic_file;
            }
        }
    }
}
