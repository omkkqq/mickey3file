package com.example.jvmfilemanager.search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BaseActivity;

import java.util.List;

public class SearchActivity extends BaseActivity implements  SearchContract.View{
    final static String SEARCH_DATA="SEARCH_DATA";
    final static String NOW_WHERE="NOW_WHERE";
    final static String SEARCH_STATUS="SEARCH_STATUS";
    SearchPresenter<SearchContract.View> presenter=new SearchPresenter<>();
    RecyclerView recyclerView;
    SearchAdapter adapter=new SearchAdapter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        recyclerView=findViewById(R.id.search_recycleView);

        presenter.onAttached(this);

        Bundle bundle = this.getIntent().getExtras().getBundle("bundle");
        String searchData=bundle.getString(SEARCH_DATA);
        String nowWhere=bundle.getString(NOW_WHERE);
        int  searchStatus=bundle.getInt(SEARCH_STATUS);
        if (searchStatus==0){
            showProgressDialog(R.string.loading);
            presenter.searchData(searchData,nowWhere);
        }
        else if (searchStatus==1){
            showProgressDialog(R.string.sql_search_loading);
            presenter.searchSQLData(searchData);
        }




    }

    @Override
    public void getSearchData(List<AllFileModel> list) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.setDataList(list);
    }
}
