package com.example.jvmfilemanager.search;

import android.util.Log;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BasePresenter;
import com.example.jvmfilemanager.db.DataRepository;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SearchPresenter<V extends SearchContract.View>extends BasePresenter<V>implements SearchContract.Presenter<V> , LoadDBListener {


    List<AllFileModel> searchFileList=new ArrayList<>();

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf+1);
    }
    Set<AllFileModel> fileSet=new HashSet<>();

    @Override
    public void searchData(String data, String whereIs) {

        char[] dataCh = new char[data.length()];
        for (int i = 0; i < data.length(); i++) {
            dataCh[i] = data.charAt(i);
        }
        File file=new File(whereIs);
        File[] files=file.listFiles();

        try {
            if (files != null && files.length > 0) {
                for (File nowFile : files) {
                    if (nowFile.isDirectory()) {
                        searchData(data,nowFile.getCanonicalPath());
                    }
                    else {
                        boolean isUnder=true;
                        for (char ch : dataCh) {
                            if (!nowFile.getName().contains(String.valueOf(ch))) {
                                isUnder = false;
                                break;
                            }
                        }
                        if (isUnder){
                            AllFileModel allFileModel = new AllFileModel();
                            allFileModel.setFilePath(nowFile.getCanonicalPath());
                            allFileModel.setFileName(nowFile.getName());
                            allFileModel.setExtension(getFileExtension(nowFile));
                            fileSet.add(allFileModel);
                        }
                    }
                }
                Log.d("fileSey", "searchData: "+fileSet);
                List<AllFileModel> list=new ArrayList<>(fileSet);
                getView().dismissProgressDialog();
                getView().getSearchData(list);
            } else if (fileSet.size() == 0) {
                getView().dismissProgressDialog();
                getView().showToast(R.string.fileNullOrfilelistNull);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            getView().dismissProgressDialog();
            getView().showToast(R.string.IOEexception);
        }
    }

    @Override
    public void searchSQLData(String data) {

        DataRepository repository=new DataRepository();
        repository.findLikeData(this,data);
    }




    //loadDb
    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {
        List<AllFileModel> allFileModels=new ArrayList<>();
        for (FavoriteEntity favoriteEntity:favoriteEntityList){
            AllFileModel allFileModel=new AllFileModel();
            allFileModel.setId(favoriteEntity.id);
            allFileModel.setExtension(favoriteEntity.fileExtension);
            allFileModel.setFilePath(favoriteEntity.filePath);
            allFileModel.setFileName(favoriteEntity.fileName);
            allFileModels.add(allFileModel);
        }
        getView().dismissProgressDialog();
        getView().getSearchData(allFileModels);
    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {
        getView().dismissProgressDialog();
        getView().showToast("Error"+error);
    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public void onGarbageDel() {

    }
}
