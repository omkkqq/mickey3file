package com.example.jvmfilemanager;


public interface EndListener {
    void onEndPicture();
    boolean onBackPressed();
}
