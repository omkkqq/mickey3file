package com.example.jvmfilemanager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.db.DataRepository;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.example.jvmfilemanager.paint.PaintActivity;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * added: 20/10/22
 * */
public class LongClickDialogFragment extends BottomSheetDialogFragment implements EndListener {
    final static String IS_BG="IS_BG";
    private String name= "null";
    private FavoriteEntity entity=new FavoriteEntity();
    private LoadDBListener loadDBListener;
    private Context context;
    private AllFileModel allFileModel;
    private String fileExtension="";
    private GarbageListener garbageListener;
    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT, -1);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.dialog_fragment_long_click, null);
        dialog.setContentView(contentView);

        TextView textFileName= dialog.findViewById(R.id.textFileItemName);
        textFileName.setText(name);



        ConstraintLayout layoutEdit= dialog.findViewById(R.id.layoutLongClickDialogEditer);
        layoutEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bottom sheet click", "onClick: Rename");
                if (fileExtension.equals("png") || fileExtension.equals("jpg")){
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(allFileModel.getFilePath(), options);
                    Intent intent=new Intent(context, PaintActivity.class);
                    ByteArrayOutputStream bs = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG,100,bs);
                    Bundle bundle=new Bundle();
                    String FILE_PATH="FILE_PATH";
                    bundle.putString(FILE_PATH,allFileModel.getFilePath());
                    bundle.putByteArray("EXTRA_BTYEARRAY", bs.toByteArray());
                    bundle.putInt(IS_BG,1);

                    intent.putExtra("undle",bundle);
                    context.startActivity(intent);
                }
                onStop();
            }
        });
        ConstraintLayout layoutRename= dialog.findViewById(R.id.layoutLongClickDialogRename);
        layoutRename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bottom sheet click", "onClick: Rename");
                onStop();
            }
        });



        ConstraintLayout layoutUpload= dialog.findViewById(R.id.layoutLongClickDialogUpload);
        layoutUpload.setOnClickListener(view -> {
            File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "garbage");

            ContextWrapper contextWrapper = new ContextWrapper(context);
            File directory = contextWrapper.getDir(context.getFilesDir().getName(), Context.MODE_PRIVATE);
            String a=folder.getAbsolutePath()+allFileModel.getFileName();
            File f =  new File(directory,"garbage");
            if (!f.exists()) {
                f.mkdirs();
            }
            Log.d("11111111111", "setupDialog: "+f.getAbsolutePath());
            moveFile(allFileModel.getFilePath(),allFileModel.getFileName(),f.getAbsolutePath(),garbageListener,allFileModel.getExtension());
            onStop();
        });



        ConstraintLayout layoutDetails= dialog.findViewById(R.id.layoutLongClickDialogDetails);
        layoutDetails.setOnClickListener(view -> {

            Log.d("bottom sheet click", "onClick: Details");
            onStop();

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            View v= LayoutInflater.from(context).inflate(R.layout.item_detail_layout,null);
            TextView v1=v.findViewById(R.id.dialogtype);
            v1.setText(allFileModel.getExtension());
            TextView v2=v.findViewById(R.id.dialogpath);
            v2.setText(allFileModel.getFilePath());
            TextView v3=v.findViewById(R.id.dialogfileSize);
            v3.setText(allFileModel.getFileGBMB());
            TextView v4=v.findViewById(R.id.dialogFolderlist);
            v4.setText(String.valueOf(allFileModel.getFileSize()));
            TextView v5=v.findViewById(R.id.dialogFileTime);
            v5.setText(setTime(allFileModel.getLastModifyDate()));
            builder.setMessage(R.string.details)
                    .setView(v)
                    .setPositiveButton(R.string.check, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {

                        }
                    });
            // Create the AlertDialog object and return it
             builder.create();
             builder.show();

        });
        ConstraintLayout layoutFavorite= dialog.findViewById(R.id.layoutLongClickDialogFavorite);
        final ImageView loveBtn=layoutFavorite.findViewById(R.id.lineFileFavorite);
        DataRepository repository=new DataRepository();

        String isRepeat=repository.findDataRepeatOrNot(entity);
        if (isRepeat==null){
            loveBtn.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }
        else {
            loveBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
        }
        layoutFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("bottom sheet click", "onClick: Details");



                DataRepository repository=new DataRepository();
                String isRepeat=repository.findDataRepeatOrNot(entity);
                boolean isLove= isRepeat != null;


                isLove=!isLove;
                Log.d("bottom sheet click", "onClick: Details"+isLove);
                if (isLove){
                    loveBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
                    repository.addItems(loadDBListener,entity);
                }
                else {
                    loveBtn.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    repository.deleteByFilePath(loadDBListener,entity);
                }

                onStop();

            }
        });
    }

    private void moveFile(String inputPath, String inputFile, String outputPath,GarbageListener listener,String fileExtension) {

        InputStream in = null;
        OutputStream out = null;
        try {

            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }
            in = new FileInputStream(inputPath );
            Log.d("a", "moveFile: "+outputPath+inputFile);
            out = new FileOutputStream(outputPath+  File.separator+inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath).delete();
            DataRepository repository=new DataRepository();
            GarbageEntity garbageEntity=new GarbageEntity();
            garbageEntity.fileName=allFileModel.getFileName();
            garbageEntity.beforeFilePath=inputPath;
            garbageEntity.afterFilePath=outputPath;
            garbageEntity.fileExtension=fileExtension;
            repository.insertGarbagesItem(loadDBListener,garbageEntity);
            listener.isGotoGarbage(allFileModel.getFileFromPath());
        }

        catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }
  public String setTime(Long modifiedTime) {
        @SuppressLint("SimpleDateFormat")
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       return format.format(new Date(modifiedTime));
    }

    public LongClickDialogFragment setName(String name, Context context) {
        this.name = name;
        this.context=context;
        return this;
    }

    public LongClickDialogFragment setGarbageListener(GarbageListener listener){
        garbageListener=listener;
        return this;
    }
    public LongClickDialogFragment setFavoriteFileNameAndPath(AllFileModel fileModel,LoadDBListener listener){
        entity.filePath=fileModel.getFilePath();
        entity.fileName=fileModel.getFileName();
        entity.id=fileModel.getId();
        entity.fileExtension=fileModel.getExtension();
        allFileModel=fileModel;
        this.fileExtension=fileModel.getExtension();
        loadDBListener=listener;
        return this;
    }


    @Override
    public void onEndPicture() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}