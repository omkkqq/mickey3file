package com.example.jvmfilemanager.home;

import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class HomePresenter {

    public HomePresenter(HomeActivity context){

    }

    public void pieChartSetting(HomeActivity context){
        context.pieChart.setUsePercentValues(true);
        context.pieChart.getDescription().setEnabled(false);
        context.pieChart.setExtraOffsets(5,10,5,5);
        context.pieChart.setCenterText("內建儲存空間\n"+Integer.toString(maxSize)+".00GB");
        context.pieChart.setDrawHoleEnabled(true);
        context.pieChart.setDrawCenterText(true);
        context.pieChart.setTransparentCircleColor(Color.BLACK);
        context.pieChart.setEntryLabelColor(Color.BLACK);
        context.pieChart.setEntryLabelTextSize(8f);
        context.pieChart.setMinimumWidth(10);
        context.pieChart.setRotationEnabled(false);

        Legend legend = context.pieChart.getLegend();
        legend.setEnabled(false);

        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(Float.valueOf(useSize),useSize+"GB已使用"));
        pieEntries.add(new PieEntry(Float.valueOf(freeSize),freeSize+"GB未使用"));

        PieDataSet pieDataSet = new PieDataSet(pieEntries,"");

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(255,0,0));
        colors.add(Color.rgb(0,255,0));

        pieDataSet.setColors(colors);
        pieDataSet.setSliceSpace(0f);
        pieDataSet.setSelectionShift(0f);
        PieData pieData = new PieData();
        pieData.setDataSet(pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());
        pieData.setValueTextSize(20f);
        pieData.setValueTextColor(Color.BLUE);
        context.pieChart.setData(pieData);
    }

    //儲存空間讀取
    public static Integer maxSize;
    public static String freeSize;
    public static String useSize;
    public void readSDCard() {
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)) {
            File sdcardDir = Environment.getExternalStorageDirectory();
            StatFs sf = new StatFs(sdcardDir.getPath());
            long blockSize = sf.getBlockSizeLong();
            long blockCount = sf.getBlockCountLong();
            long availCount = sf.getAvailableBlocksLong();
            int storageMaxSize = Math.round(blockSize*blockCount/1000000000);;
            Double storageFreeSize = integerDivision((availCount*blockSize),1000000000,2);
            freeSize = Double.toString(storageFreeSize);
            maxSize = 0;
            if(storageMaxSize<=8)
                maxSize = 8;
            else if(storageMaxSize > 8 && storageMaxSize <= 16)
                maxSize = 16;
            else if(storageMaxSize > 16 && storageMaxSize <= 32)
                maxSize = 32;
            else if(storageMaxSize > 32 && storageMaxSize <= 64)
                maxSize = 64;
            else if(storageMaxSize > 64 && storageMaxSize <= 128)
                maxSize = 128;
            else if(storageMaxSize > 128 && storageMaxSize <= 256)
                maxSize = 256;
            else if(storageMaxSize > 256 && storageMaxSize <= 512)
                maxSize = 512;
            else maxSize = 1024;

            DecimalFormat decimalFormat = new DecimalFormat(".00");
            Double UseSize = maxSize - storageFreeSize;
            useSize = decimalFormat.format(UseSize);

            Log.d("123", "block大小:"+ blockSize+",block数目:"+ blockCount+",总大小:"+blockSize*blockCount);
            Log.d("123", "可用的block数目：:"+ availCount+",剩余空间:"+ availCount*blockSize);
            Log.d("123",Integer.toString(maxSize));
            Log.d("123",freeSize);
            Log.d("123",useSize);
        }
    }

    //整數除法(數字1,數字2,要取到小數第幾位)
    public static double integerDivision(double v1, double v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "小數位數必須為正整數或零");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

}
