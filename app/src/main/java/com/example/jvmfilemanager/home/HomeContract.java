package com.example.jvmfilemanager.home;

import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;

import java.util.LinkedHashMap;
import java.util.List;

public interface HomeContract {

    interface View extends BaseContract{
        void onDataPass(String data);
    }

    interface Presenter<V extends  View> extends BaseAttachImpl<V>{


    }
}
