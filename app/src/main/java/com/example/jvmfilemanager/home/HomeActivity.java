package com.example.jvmfilemanager.home;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.jvmfilemanager.EndListener;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFragment;
import com.example.jvmfilemanager.base.BaseActivity;
import com.example.jvmfilemanager.base.SettingPreference;
import com.example.jvmfilemanager.base.SettingPreferenceProvider;
import com.example.jvmfilemanager.favorite.FavoriteFragment;
import com.example.jvmfilemanager.garbage.GarbageFragment;
import com.example.jvmfilemanager.images.ImgsFragment;
import com.example.jvmfilemanager.music.MusicFragment;
import com.example.jvmfilemanager.paint.PaintActivity;
import com.example.jvmfilemanager.sdcard.SdcardFragment;
import com.example.jvmfilemanager.search.SearchActivity;
import com.example.jvmfilemanager.settings.SettingsActivity;
import com.example.jvmfilemanager.videos.VideosFragment;
import com.github.mikephil.charting.charts.PieChart;
import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.io.IOException;


public class HomeActivity extends BaseActivity implements HomeContract.View{

    final static String SEARCH_DATA="SEARCH_DATA";
    final static String IS_BG="IS_BG";
    final static String NOW_WHERE="NOW_WHERE";
    final static String FILE_PATH="FILE_PATH";
    final static String SEARCH_STATUS="SEARCH_STATUS";
    AllFragment allFragment;
    FragmentManager fragmentManager;
     FragmentTransaction fragmentTransaction ;
    EditText searchText;
    Button findBtn;
    String nowWhere="";
    int  REQUEST_EXTERNAL_STORAGE=0;
    private Button allBtn;
    private Button testBtn,getTestBtn;
    Context context;
    private  int searchStatus=0;
    private String ffPath;
    int sendtoAll=0;
    Bundle savedInstanceState;
    Button sdCardBtn,imgBtn,videoBtn,musicBtn;
    SettingPreferenceProvider settingPreference;
    Button garBageBtn;
    private HomePresenter homePresenter;
    public PieChart pieChart;

    @Override
    public void init() {
        super.init();
        testBtn=findViewById(R.id.button);
        allBtn=findViewById(R.id.allBtn);
        searchText=findViewById(R.id.searchText);
        findBtn=findViewById(R.id.findBtn);
        getTestBtn=findViewById(R.id.button2);
        imgBtn=findViewById(R.id.imgBtn);
        pieChart = findViewById(R.id.pie_chart_storage);
        context=this;
        settingPreference=new SettingPreference(this);

        musicBtn=findViewById(R.id.musicBtn);
        sdCardBtn=findViewById(R.id.sdcardBtn);
        videoBtn=findViewById(R.id.VideoBtn);
        garBageBtn=findViewById(R.id.garbageBtn);
        getTestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                settingPreference.setFontSize(2F);
            }
        });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState=savedInstanceState;
        setContentView(R.layout.activity_home);
        context=this;
        homePresenter = new HomePresenter(this);
        init();
        onClickListener();
        getPermission();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt("edttext", sendtoAll);
        // set Fragmentclass Arguments
        allFragment = new AllFragment();
        allFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment_layout, allFragment, "HOME");
        fragmentTransaction.commit();
        try {
            createGarbageFolder();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void createGarbageFolder() throws IOException {
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "garbage");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            Log.d("createGarbageFolder", "createGarbageFolder: "+success);
        } else {
            // Do something else on failure
        }
    }

    private void onClickListener() {


        garBageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                GarbageFragment garbageFragment = new GarbageFragment();
                fragmentTransaction.replace(R.id.fragment_layout, garbageFragment, "GARBAGE");
                fragmentTransaction.commit();
            }
        });
        testBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt(IS_BG,0);
                bundle.putString(FILE_PATH,ffPath);
                Intent intent=new Intent(HomeActivity.this, PaintActivity.class);
                intent.putExtra("undle",bundle);
                context.startActivity(intent);
                sendtoAll=1;
            }
        });

        nowWhere= Environment.getExternalStorageDirectory().getAbsolutePath();
        findBtn.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String searchData=searchText.getText().toString();
                Bundle bundle=new Bundle();
                bundle.putString(SEARCH_DATA,searchData);
                bundle.putString(NOW_WHERE,nowWhere);
                bundle.putInt(SEARCH_STATUS,searchStatus);
                Intent intent=new Intent(HomeActivity.this,SearchActivity.class);
                intent.putExtra("bundle",bundle);
                context.startActivity(intent);
            }
        });

        videoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                VideosFragment videosFragment = new VideosFragment();
                fragmentTransaction.replace(R.id.fragment_layout, videosFragment, "SDCARD");
                fragmentTransaction.commit();
            }
        });

        musicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                MusicFragment musicFragment = new MusicFragment();
                fragmentTransaction.replace(R.id.fragment_layout, musicFragment, "Music");
                fragmentTransaction.commit();
            }
        });
        searchStatus=0;



        // home navigation drawer added on 20/10/22
        final DrawerLayout drawerLayout = findViewById(R.id.drawerLayoutMain);
        NavigationView navigationView = findViewById(R.id.navigationViewMain);
        Button btnDrawer= findViewById(R.id.btnMainDrawer);
        btnDrawer.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RtlHardcoded")
            @Override
            public void onClick(View view) {
                if(drawerLayout.isDrawerOpen(Gravity.LEFT))
                    drawerLayout.closeDrawer(Gravity.LEFT);
                else
                    drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        sdCardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SdcardFragment sdcardFragment = new SdcardFragment();
                fragmentTransaction.replace(R.id.fragment_layout, sdcardFragment, "SDCARD");
                fragmentTransaction.commit();
            }
        });

        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ImgsFragment imgsFragment = new ImgsFragment();
                fragmentTransaction.replace(R.id.fragment_layout, imgsFragment, "Img");
                fragmentTransaction.commit();
            }
        });
        allBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AllFragment allFragment = new AllFragment();
                fragmentTransaction.replace(R.id.fragment_layout, allFragment, "HOME");
                fragmentTransaction.commit();
            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                switch(item.getItemId()){
                    case R.id.nav_item_home:
                        AllFragment allFragment = new AllFragment();
                        fragmentTransaction.replace(R.id.fragment_layout, allFragment, "HOME");
                        fragmentTransaction.commit();
                        break;
                    case R.id.nav_item_screenshot:
                        Log.d("Navigation item", "screenshot");
                        break;
                    case R.id.nav_item_settings:
                        Log.d("Navigation item", "settings");

                        startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
                        finish();
                        break;
                    case R.id.nav_item_favorite:
                        searchStatus=1;
                        FavoriteFragment favoriteFragment = new FavoriteFragment();
                        fragmentTransaction.replace(R.id.fragment_layout, favoriteFragment, "HOME");
                        fragmentTransaction.commit();
                }
                drawerLayout.closeDrawer(Gravity.LEFT);
                return false;
            }
        });

        //MPAndroidchart 圓餅圖
        homePresenter.readSDCard();
        homePresenter.pieChartSetting(this);

    }

    private void getPermission() {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[] {
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL_STORAGE
            );
        }else{
            //writeFile();
            Toast.makeText(this, "有權限(test)" ,Toast.LENGTH_SHORT).show();
        }
    }
    private EndListener listener;
    @Override
    protected void onRestart() {
        super.onRestart();
        gotoAllFragment(listener);

    }
//    public void setActivityListener(EndListener activityListener) {
//        this.listener = activityListener;
//    }
    private void gotoAllFragment(EndListener listener) {
        listener.onEndPicture();
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        super.onAttachFragment(fragment);
        try {
            this.listener =(EndListener)fragment;
        }catch (Exception e){
            Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }
       
    }

    @Override
    public void onDataPass(String data) {
        ffPath=data;
    }

    @Override
    public void onBackPressed() {

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
        if (!(fragment instanceof EndListener) || !((EndListener) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }
}
