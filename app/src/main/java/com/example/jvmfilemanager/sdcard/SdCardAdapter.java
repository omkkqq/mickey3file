package com.example.jvmfilemanager.sdcard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jvmfilemanager.LongClickDialogFragment;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class SdCardAdapter extends RecyclerView.Adapter<SdCardAdapter.ViewHolder> implements SdCardContract.Adapter, LoadDBListener {
    private List<AllFileModel> dataList=new ArrayList<>();
    Context context;
    private LoadDBListener loadDBListener;

    public SdCardAdapter(SdCardPresenter<SdCardContract.View> presenter) {
        this.presenter = presenter;
    }

    private SdCardPresenter<SdCardContract.View> presenter;
    @NonNull
    @Override
    public SdCardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View itemView=inflater.inflate(R.layout.item_all,parent,false);
        context=parent.getContext();
        loadDBListener=this;
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SdCardAdapter.ViewHolder holder, int position) {
        holder.bind(dataList.get(position));
        dataList.get(position).setPosition(position);
    }

    @Override
    public int getItemCount() {
        return dataList==null?0:dataList.size();
    }

    @Override
    public void setDataList(List<AllFileModel> list) {
        this.dataList=list;
        notifyDataSetChanged();
    }

    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onComplete(String status) {

    }

    @Override
    public void onDeleteComplete() {

    }

    @Override
    public void onGarbageDel() {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView fileName;
        TextView filePath;
        ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fileName=itemView.findViewById(R.id.gabName);
            imageView=itemView.findViewById(R.id.gabImg);
        }

        private void bind(final AllFileModel fileModel){
            fileName.setText(fileModel.getFileName());
            imageView.setImageResource(setFileImg(fileModel));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        presenter.getNowPathMoreData(context,fileModel.getFilePath());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            // btn long click listener, added: 20/10/22
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    BottomSheetDialogFragment bottomSheet = new LongClickDialogFragment()
                            .setName(fileModel.getFileName(),context)
                            .setFavoriteFileNameAndPath(fileModel,loadDBListener);
                    bottomSheet.show(presenter.getFragmentManager(), bottomSheet.getTag());
                    return false;
                }
            });
        }

        private int setFileImg(AllFileModel allFileModel){
//            #FF6F00
            if (allFileModel.getExtension().equals("directory")){
                return R.drawable.ic_folder;
            }
            else if (allFileModel.getExtension().equals("pdf")){
                return R.drawable.ic_picture_as_pdf_black_24dp;
            }
            else if (allFileModel.getExtension().equals("zip") || allFileModel.getExtension().equals("7z")){
                return R.drawable.ic_fzip;
            }
            else if (allFileModel.getExtension().equals("tar")){
                return R.drawable.ic_tar_file_format_symbol;
            }
            else if (allFileModel.getExtension().equals("csv")){
                return R.drawable.ic_csv_file_format_extension;
            }
            else if (allFileModel.getExtension().equals("bmp")){
                return R.drawable.ic_bmp_file_format_symbol;
            }
            else if (allFileModel.getExtension().equals("apk")){
                return R.drawable.ic_android_black_24dp;
            }
            else if (allFileModel.getExtension().equals("jpg") || allFileModel.getExtension().equals("png")){
                return R.drawable.ic_picture;
            }
            else if (allFileModel.getExtension().equals("mp3") || allFileModel.getExtension().equals("mp4")
                    || allFileModel.getExtension().equals("wav")){
                return R.drawable.ic_music;
            }
            else {
                return R.drawable.ic_file;
            }
        }
    }
}
