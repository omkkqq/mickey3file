package com.example.jvmfilemanager.sdcard;

import android.content.Context;
import android.net.Uri;

import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.favorite.FavoriteContract;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

public interface SdCardContract {
    interface View extends BaseContract {
        void getDataList(List<AllFileModel> list,String path);
        void openIntent(File file);
        void updateLinkedMap(LinkedHashMap<String,List<AllFileModel>> linkedHashMap);

    }

    interface Adapter {
        void setDataList(List<AllFileModel> list);
    }

    interface Presenter<V extends SdCardContract.View> extends BaseAttachImpl<V> {
        void getNowPathMoreData(Context context, Uri path);
        void getClickMoreData(String path,LinkedHashMap<String,List<AllFileModel>> linkedHashMap);
    }
}
