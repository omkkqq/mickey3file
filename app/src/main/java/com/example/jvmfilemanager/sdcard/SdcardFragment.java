package com.example.jvmfilemanager.sdcard;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.UriPermission;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.Toast;

import com.example.jvmfilemanager.EndListener;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.all.BtnPlusAdapter;
import com.example.jvmfilemanager.base.BaseFragment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static android.app.Activity.RESULT_OK;


public class SdcardFragment extends BaseFragment implements EndListener,SdCardContract.View {
    private LinkedHashMap<String,List<AllFileModel>> listLinkedHashMap=new LinkedHashMap<>();
    public static final int REQ_PICK_DIRECTORY=0;
    public static final  int REQ_SD_CARD_ACCESS=1;
    private Context context;
    private RecyclerView recyclerView;
    private SdCardAdapter adapter;
    private SdCardPresenter<SdCardContract.View> presenter=new SdCardPresenter<>();
    private  String path="";
    private Button backBtn;
    private BtnPlusAdapter btnPlusAdapter;
    RecyclerView autoPlusLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        context=container.getContext();

        View v=inflater.inflate(R.layout.fragment_sdcard, container, false);
        recyclerView=v.findViewById(R.id. sdRecycleView);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        backBtn=view.findViewById(R.id.back);
        presenter.onAttached(this);
        autoPlusLayout = view.findViewById(R.id.btnpluslayout);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listLinkedHashMap.size()>1){
                    try {
                        presenter.getNowPathMoreData(context,getLastKey(listLinkedHashMap));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    showToast(R.string.isLastKey);
                }
            }
        });

        presenter.onAttached(this);
        presenter.setFragmentManager(getFragmentManager());
        try {
            requestPermission();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onViewCreated(view, savedInstanceState);
    }

    void requestPermission() throws Exception {
        if(Build.VERSION.SDK_INT < 24){
            startActivityForResult(new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE), REQ_PICK_DIRECTORY);
            return;
        }

        // find removable device using getStorageVolumes
        StorageManager sm = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        List<StorageVolume> storageVolumes = sm.getStorageVolumes();
        for (StorageVolume st:storageVolumes){
            boolean sdCard=st.isRemovable();
            if(sdCard){
               // st.getUuid()
                path="/storage/"+st.getUuid();

                presenter.getNowPathMoreData(context,"/storage/"+st.getUuid());
              startActivityForResult(st.createAccessIntent(null), REQ_SD_CARD_ACCESS);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d("onActivityResult", "onActivityResult: " + requestCode + requestCode + resultCode);
        if (requestCode == REQ_SD_CARD_ACCESS || requestCode == REQ_PICK_DIRECTORY) {
            if (resultCode == RESULT_OK) {
                if (data == null) {
                    Toast.makeText(context, "Error obtaining access", Toast.LENGTH_SHORT).show();
                } else {
                    Uri sdCardUri = data.getData();
                    presenter.getNowPathMoreData(context,sdCardUri);
                }
            } else {
                Toast.makeText(context, "access denied", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void getDataList(List<AllFileModel> list,String nowPath) {

        btnPlusAdapter=new BtnPlusAdapter(presenter,1);
        autoPlusLayout.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false));
        autoPlusLayout.setAdapter(btnPlusAdapter);
        btnPlusAdapter.setDataList(nowPath,listLinkedHashMap);


        adapter=new SdCardAdapter(presenter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setDataList(list);

        listLinkedHashMap.put(path,list);
        path=nowPath;
    }

    @Override
    public void openIntent(File f) {

        Log.d("qqqqqqqq", "openIntent: "+getFileExtension(f));
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType( FileProvider.getUriForFile(getContext(), getActivity().getApplicationContext().getPackageName() + ".provider", f), MimeTypeMap.getSingleton().getMimeTypeFromExtension(getFileExtension(f)));
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(i);
    }

    @Override
    public void updateLinkedMap(LinkedHashMap<String, List<AllFileModel>> linkedHashMap) {

    }

    public String getLastKey(LinkedHashMap<String,List<AllFileModel>> ll) {
        listLinkedHashMap.remove(path);
        String out="";
        for (String key : ll.keySet()) {
            out = key;
        }
        return out;
    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "directory"; // empty extension
        }
        return name.substring(lastIndexOf+1);
    }


    @Override
    public void onEndPicture() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void init() {

    }


}

