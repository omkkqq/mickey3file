package com.example.jvmfilemanager.sdcard;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.FragmentManager;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.all.AllFileModel;
import com.example.jvmfilemanager.base.BasePresenter;
import com.example.jvmfilemanager.favorite.FavoriteContract;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

public class SdCardPresenter<V extends SdCardContract.View> extends BasePresenter<V> implements SdCardContract.Presenter<V> {
//    @Override
//    public void getNowPathMoreData(Context context, Uri sdCardUri) {
//        DocumentFile fromDir = DocumentFile.fromTreeUri(context, sdCardUri);
//        String path = FileUtil.getFullPathFromTreeUri(sdCardUri,context);
//        List<AllFileModel> list=new ArrayList<>();
//        for (DocumentFile file : fromDir.listFiles()) {
//
//            AllFileModel allFileModel=new AllFileModel();
//            allFileModel.setFileName(file.getName());
//            allFileModel.setExtension(file.getType());
//            allFileModel.setFilePath(path+"/"+file.getName());
//
//            list.add(allFileModel);
//            Log.d("StorageAccess", "obtained access to " + FileUtil.getFullPathFromTreeUri(file.getUri(),context)+"/"+file.getName());
//        }
//        getView().getDataList(list);
//    }

    public void getNowPathMoreData(Context context,String sdCardUri) throws Exception {
//        DocumentFile fromDir = DocumentFile.fromTreeUri(context, sdCardUri);
//        String path = FileUtil.getFullPathFromTreeUri(sdCardUri,context);
        File fromDir=new File(sdCardUri);
        List<AllFileModel> list=new ArrayList<>();
        if (fromDir.listFiles()!=null && (fromDir.listFiles().length>0)){
            for (File nowFile : fromDir.listFiles()) {
                AllFileModel allFileModel=new AllFileModel();
                allFileModel.setFileName(nowFile.getName());
                allFileModel.setExtension(getFileExtension(nowFile));
                allFileModel.setFilePath(nowFile.getCanonicalPath());
                allFileModel.setLastModifyDate(nowFile.lastModified());
                allFileModel.setFileLength(nowFile.length());
                allFileModel.setFileGBMB(formetFileSizeToGBMB(allFileModel.getFileLength()));
                if (nowFile.isDirectory()) {
                    allFileModel.setDirectly(true);
                    allFileModel.setFileSize(getFilelistSize(nowFile));
                    allFileModel.setFileLength(getFileSize(nowFile));
                    allFileModel.setFileGBMB(formetFileSizeToGBMB(allFileModel.getFileLength()));
                }
                list.add(allFileModel);
            }
            getView().getDataList(sortFileByExtension(list),sdCardUri);
        }
        else if ( list.size()==0) {
            getView().openIntent(fromDir);
        }

    }


    private List<AllFileModel> sortFileByExtension(List<AllFileModel> list){
        Collections.sort(list, new Comparator<AllFileModel>() {
            @Override
            public int compare(AllFileModel o1, AllFileModel o2) {
                if (o1.getExtension().equals("directory") && o2.getExtension().equals("directory")){
                    return o1.getFileName().toUpperCase().compareTo(o2.getFileName().toUpperCase());
                }
                if (o1.getExtension().equals("directory") && !o2.getExtension().equals("directory"))
                    return -1;
                if (!o1.getExtension().equals("directory") && o2.getExtension().equals("directory"))
                    return 1;
                return o1.getExtension().compareTo(o2.getExtension());
            }
        });
        return list;
    }
    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "directory"; // empty extension
        }
        return name.substring(lastIndexOf+1);
    }
    //取得資料夾大小
    private long getFileSize(File f)throws Exception {
        long size = 0;
        File flist[] = f.listFiles();
        for (int i = 0; i < flist.length; i++)
        {
            if (flist[i].isDirectory())
            {
                size = size + getFileSize(flist[i]);
            } else
            {
                size = size + flist[i].length();
            }
        }
        return size;
    }

    //轉換檔案大小為GB/MB
    public String formetFileSizeToGBMB(long fileS) {//轉換檔案大小
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "K";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "G";
        }
        return fileSizeString;
    }

    //取得檔案數量
    private int getFilelistSize(File f){//遞迴求取目錄檔案個數
        int size = 0;
        File flist[] = f.listFiles();
        size=flist.length;
        for (int i = 0; i < flist.length; i++) {
            if (flist[i].isDirectory()) {
                size = size + getFilelistSize(flist[i]);
                size--;
            }
        }
        return size; }
    // added: 20/10/22
    private FragmentManager fragmentManager;

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void getNowPathMoreData(Context context, Uri path) {

    }

    @Override
    public void getClickMoreData(String path, LinkedHashMap<String, List<AllFileModel>> linkedHashMap) {
        getView().showProgressDialog(R.string.loading);
        File file=new File(path);
        File[] files=file.listFiles();
        List<AllFileModel> fileList=new ArrayList<>();
        try {
            if (files!=null){
                for (File nowFile : files){
                    AllFileModel allFileModel=new AllFileModel();
                    allFileModel.setFilePath(nowFile.getCanonicalPath());
                    allFileModel.setFileName(nowFile.getName());
                    allFileModel.setExtension(getFileExtension(nowFile));
                    if (nowFile.isDirectory()) allFileModel.setDirectly(true);
                    fileList.add(allFileModel);
                }
                Log.d("22222", "getNowPathMoreData: "+fileList);
                getView().dismissProgressDialog();
                getView().updateLinkedMap(linkedHashMap);
                getView().getDataList(fileList,path);

            }else if ( fileList.size()==0){
                getView().showToast(R.string.fileNullOrfilelistNull);
                getView().dismissProgressDialog();
            }
        } catch (IOException e) {
            e.printStackTrace();
            getView().showToast(R.string.IOEexception);
            getView().dismissProgressDialog();
        }
    }
}
