package com.example.jvmfilemanager.all;

import android.net.Uri;
import android.util.Log;

public class AllFileModel {



    private String fileFromPath="";
    private String extension="";
    private Boolean isDirectly=false;
    private String fileName="";
    private String filePath;
    private Boolean isSelected=false;
    private Integer imgViewResource=0;
    private Integer position=0;
    private Long  lastModifyDate=0L;
    private Long  fileLength=0L;
    private String fileGBMB="0MB";
    private int fileSize=1;

    public String getFileGBMB() {
        return fileGBMB;
    }

    public void setFileGBMB(String fileGBMB) {
        this.fileGBMB = fileGBMB;
    }

    public String getFileFromPath() {
        return fileFromPath;
    }

    public void setFileFromPath(String fileFromPath) {
        this.fileFromPath = fileFromPath;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }




    public Long getFileLength() {
        return fileLength;
    }

    public void setFileLength(Long fileLength) {
        this.fileLength = fileLength;
    }



    public Long getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Long lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private Integer id=0;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    private Uri uri;
    //----------------------------
    public Boolean getDirectly() {
        return isDirectly;
    }

    public void setDirectly(Boolean directly) {
        isDirectly = directly;
    }
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public Integer getImgViewResource() {
        return imgViewResource;
    }

    public void setImgViewResource(Integer imgViewResource) {
        this.imgViewResource = imgViewResource;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }


}
