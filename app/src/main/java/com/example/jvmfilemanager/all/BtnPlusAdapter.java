package com.example.jvmfilemanager.all;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jvmfilemanager.LongClickDialogFragment;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.sdcard.SdCardContract;
import com.example.jvmfilemanager.sdcard.SdCardPresenter;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class BtnPlusAdapter extends RecyclerView.Adapter<BtnPlusAdapter.ViewHolder> implements  AllFragmentContract.BtnAdapter {


    private List<String> btnList=new ArrayList<>();
    private LinkedHashMap<String,List<AllFileModel>> linkedHashMap=new LinkedHashMap<>();
    private  AllFragmentPresenter presenter;
    private int where=0;

    public BtnPlusAdapter(SdCardPresenter<SdCardContract.View> allPresenter,int where) {
        this.allPresenter = allPresenter;
        this.where=where;
    }

    private SdCardPresenter<SdCardContract.View> allPresenter;
    public BtnPlusAdapter() {

    }

    public BtnPlusAdapter(AllFragmentPresenter presenter,int where) {
        this.presenter = presenter;
        this.where=where;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View itemView=inflater.inflate(R.layout.item_btn_autocreate,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(btnList.get(position),position,linkedHashMap);
    }

    @Override
    public int getItemCount() {
        return btnList==null?0:btnList.size();
    }


    @Override
    public void setDataList(String nowPath, LinkedHashMap<String,List<AllFileModel>> linkedHashMap) {
        this.linkedHashMap=linkedHashMap;
        btnList.clear();
        if (where==0){
            String splitPath=nowPath.replace("/storage/emulated/0","");
            for (String oneObjPath:splitPath.split("/")){
                btnList.add(oneObjPath);
            }
        }
        else if (where==1){
            String splitPath=nowPath.replace("/storage/","");
            for (String oneObjPath:splitPath.split("/")){
                btnList.add(oneObjPath);
            }
        }

        notifyDataSetChanged();
    }

    class ViewHolder extends  RecyclerView.ViewHolder {

        TextView btn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btn=itemView.findViewById(R.id.autoBtn);
        }

        @SuppressLint({"SetTextI9n", "SetTextI18n"})
        private void bind(String clipPath, final int position, final LinkedHashMap<String,List<AllFileModel>> linkedHashMap){
            btn.setText("         "+clipPath+"         "+"/");
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (where==0){
                        presenter.getClickMoreData(
                                getSpeficKey(linkedHashMap,position),getNowLinkedHashMap(linkedHashMap,position)
                        );
                        presenter=null;
                    }
                    else  if (where==1){
                        allPresenter.getClickMoreData(
                                getSpeficKey(linkedHashMap,position),getNowLinkedHashMap(linkedHashMap,position)
                        );
                        allPresenter=null;
                    }

                }
            });


            //presenter.getNowPathMoreData(clipPath);
        }

        public String getSpeficKey(LinkedHashMap<String,List<AllFileModel>> ll,Integer position) {
            int nowPostion=0;
            String out="";
            for (String key : ll.keySet()) {

                if (nowPostion==position){
                    out=key;
                    break;
                }
                nowPostion++;

            }
            return out;
        }

        public LinkedHashMap<String,List<AllFileModel>> getNowLinkedHashMap(LinkedHashMap<String,List<AllFileModel>> ll,int position){
            String out="";
            int nowPostion=ll.size()-1;

            for (int i=0;i<ll.size();i++) {
                if (nowPostion>position){
                    for (String key1 : ll.keySet()) {
                        out=key1;
                    }
                    ll.remove(out);
                    nowPostion--;
                }
            }
            return ll;
        }

    }
}
