package com.example.jvmfilemanager.all;

import android.content.Context;

import androidx.paging.DataSource;

import java.util.List;

public class AllDataSourceFactory extends androidx.paging.DataSource.Factory {

    Context context;
    List<AllFileModel> allFileModels;
    public AllDataSourceFactory(Context context, List<AllFileModel> allFileModels) {
        this.context=context;
        this.allFileModels=allFileModels;
    }
    @Override
    public DataSource create() {
        return new AllDataSource(allFileModels);


    }
}
