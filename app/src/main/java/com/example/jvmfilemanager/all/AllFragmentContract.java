package com.example.jvmfilemanager.all;

import android.content.Intent;

import com.example.jvmfilemanager.base.BaseAttachImpl;
import com.example.jvmfilemanager.base.BaseContract;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

public interface AllFragmentContract {



    interface View extends BaseContract {
        void getNoData(String path);
        void getDataFromPath(List<AllFileModel> list,String nowPath);
        void updateLinkedMap(LinkedHashMap<String,List<AllFileModel>> linkedHashMap);
        void openIntent(File f);
    }

    interface Adapter{
        void setDataList(List<AllFileModel> list);
    }

    interface BtnAdapter{
        void setDataList(String nowPath, LinkedHashMap<String,List<AllFileModel>> linkedHashMap);
    }

    interface  Presenter<V extends  View> extends BaseAttachImpl<V> {
        void getNowPathMoreData(String path);
        void getClickMoreData(String path,LinkedHashMap<String,List<AllFileModel>> linkedHashMap);
    }
}
