package com.example.jvmfilemanager.all;

public class BtnAutoData {
    public String getBtnAutoData() {
        return btnAutoData;
    }

    public void setBtnAutoData(String btnAutoData) {
        this.btnAutoData = btnAutoData;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private String btnAutoData="";
    private int position=0;
}
