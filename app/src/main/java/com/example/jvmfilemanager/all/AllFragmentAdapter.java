package com.example.jvmfilemanager.all;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jvmfilemanager.GarbageListener;
import com.example.jvmfilemanager.LongClickDialogFragment;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.db.FavoriteEntity;
import com.example.jvmfilemanager.db.GarbageEntity;
import com.example.jvmfilemanager.db.LoadDBListener;
import com.example.jvmfilemanager.images.Images;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class AllFragmentAdapter extends PagedListAdapter<AllFileModel, AllFragmentAdapter.ViewHolder> implements  AllFragmentContract.Adapter, LoadDBListener, GarbageListener {

    List<AllFileModel> list=new ArrayList<>();
    AllFragmentPresenter presenter=new AllFragmentPresenter();
    Context context;

    public AllFragmentAdapter() {
        super(mDiffCallback);
    }

    public void setPresenter(AllFragmentPresenter presenter){
        this.presenter=presenter;
    }

    private static final  DiffUtil.ItemCallback<AllFileModel> mDiffCallback = new DiffUtil.ItemCallback<AllFileModel>() {
        @Override
        public boolean areItemsTheSame(@NonNull AllFileModel oldItem,@NonNull AllFileModel newItem) {
            return oldItem.getId() == newItem.getId();
        }
        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull AllFileModel oldItem,@NonNull AllFileModel newItem) {
            return (oldItem == newItem);
        }
    };



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View itemView=inflater.inflate(R.layout.item_all,parent,false);
        context=parent.getContext();
        return new ViewHolder(itemView,this,this);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AllFileModel allFileModel = getItem(position);
        allFileModel.setPosition(position);
        holder.bind(allFileModel);
    }



    @Override
    public void setDataList(List<AllFileModel> list) {
        this.list=list;
        notifyDataSetChanged();
    }

    @Override
    public void onFavoriteFinished(List<FavoriteEntity> favoriteEntityList) {

    }

    @Override
    public void onGarbageFinished(List<GarbageEntity> garbageEntityList) {
        Log.d("onGarbageFinished", "onGarbageFinished: ");
    }

    @Override
    public void onError(String error) {
        presenter.getView().showToast(error);
    }

    @Override
    public void onComplete(String status) {
        if (status.equals(presenter.getView().getResourceString(R.string.db_update))){
            presenter.getView().showToast(R.string.db_add_ok);
        }
        else if (status.equals(presenter.getView().getResourceString(R.string.db_update))){
            presenter.getView().showToast(R.string.db_update_ok);
        }


    }

    @Override
    public void onDeleteComplete() {
        presenter.getView().showToast(R.string.db_delete_ok);
    }

    @Override
    public void onGarbageDel() {

    }

    @Override
    public void isGotoGarbage(String path) {
        presenter.getNowPathMoreData(path);
    }


    class ViewHolder extends  RecyclerView.ViewHolder {
        TextView fileName;
        TextView filePath;
        ImageView imageView;
        LoadDBListener loadDBListener;
        GarbageListener garbageListener;
        public ViewHolder(@NonNull View itemView,LoadDBListener listener,GarbageListener garbageListener) {
            super(itemView);
            fileName=itemView.findViewById(R.id.gabName);
            loadDBListener=listener;
            imageView=itemView.findViewById(R.id.gabImg);
            this.garbageListener=garbageListener;
        }

        private void bind(final AllFileModel fileModel){
            fileName.setText(fileModel.getFileName());
            imageView.setImageResource(setFileImg(fileModel));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    presenter.getNowPathMoreData(fileModel.getFilePath());
                }
            });

            // btn long click listener, added: 20/10/22
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    BottomSheetDialogFragment bottomSheet = new LongClickDialogFragment()
                            .setName(fileModel.getFileName(),context)
                            .setFavoriteFileNameAndPath(fileModel,loadDBListener)
                            .setGarbageListener(garbageListener);
                    bottomSheet.show(presenter.getFragmentManager(), bottomSheet.getTag());
                    return false;
                }
            });
        }


        private int setFileImg(AllFileModel allFileModel){
//            #FF6F00
            if (allFileModel.getExtension().equals("directory")){
                return R.drawable.ic_folder;
            }
            else if (allFileModel.getExtension().equals("pdf")){
                return R.drawable.ic_picture_as_pdf_black_24dp;
            }
            else if (allFileModel.getExtension().equals("zip") || allFileModel.getExtension().equals("7z")){
                return R.drawable.ic_fzip;
            }
            else if (allFileModel.getExtension().equals("tar")){
                return R.drawable.ic_tar_file_format_symbol;
            }
            else if (allFileModel.getExtension().equals("csv")){
                return R.drawable.ic_csv_file_format_extension;
            }
            else if (allFileModel.getExtension().equals("bmp")){
                return R.drawable.ic_bmp_file_format_symbol;
            }
            else if (allFileModel.getExtension().equals("apk")){
                return R.drawable.ic_android_black_24dp;
            }
            else if (allFileModel.getExtension().equals("jpg") || allFileModel.getExtension().equals("png")){
                return R.drawable.ic_picture;
            }
            else if (allFileModel.getExtension().equals("mp3") || allFileModel.getExtension().equals("mp4")
            || allFileModel.getExtension().equals("wav")){
                return R.drawable.ic_music;
            }
            else {
                return R.drawable.ic_file;
            }
        }
    }
}
