package com.example.jvmfilemanager.all;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.jvmfilemanager.EndListener;
import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.BaseFragment;
import com.example.jvmfilemanager.home.HomeContract;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


public class AllFragment extends BaseFragment implements AllFragmentContract.View, EndListener {

    private LinkedHashMap<String,List<AllFileModel>> listLinkedHashMap=new LinkedHashMap<>();
    private RecyclerView allRecycleView;
    private Button backBtn;
    private AllFragmentPresenter<AllFragmentContract.View> presenter=new AllFragmentPresenter<>();
    private AllFragmentAdapter adapter;
    private BtnPlusAdapter btnPlusAdapter;
    private Context context;
    int REQUEST_EXTERNAL_STORAGE=0;
    @Override
    public void init() {

    }



    HomeContract.View dataPasser;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (HomeContract.View) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // ((HomeActivity) getActivity()).setActivityListener(AllFragment.this);
        // give presenter fragment manager, added on 20/10/22
        presenter.setFragmentManager(getFragmentManager());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=container.getContext();
        return inflater.inflate(R.layout.fragment_all, container, false);
    }
    String path;
    RecyclerView autoPlusLayout;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();


        allRecycleView=view.findViewById(R.id.all_recyclerview);
        backBtn=view.findViewById(R.id.back);
        presenter.onAttached(this);
        autoPlusLayout = view.findViewById(R.id.btnpluslayout);
        String.valueOf(Environment.getExternalStorageDirectory());

        path= Environment.getExternalStorageDirectory().getAbsolutePath();
        presenter.getNowPathMoreData(path);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listLinkedHashMap.size()>1){
                    presenter.getNowPathMoreData(getLastKey(listLinkedHashMap));
                }
                else {
                    showToast(R.string.isLastKey);
                }
            }
        });
    }

    public String getLastKey(LinkedHashMap<String,List<AllFileModel>> ll) {
        listLinkedHashMap.remove(path);
        String out="";
        for (String key : ll.keySet()) {
            out = key;
        }
        return out;
    }

    @Override
    public void getNoData(String nowPath) {
        btnPlusAdapter=new BtnPlusAdapter(presenter,0);
        autoPlusLayout.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false));
        autoPlusLayout.setAdapter(btnPlusAdapter);
        listLinkedHashMap.put(nowPath,new ArrayList<>());
        btnPlusAdapter.setDataList(path,listLinkedHashMap);
        allRecycleView.setVisibility(View.GONE);
        path=nowPath;
        dataPasser.onDataPass(path);
    }

    @Override
    public void getDataFromPath(List<AllFileModel> list,String nowPath) {

        btnPlusAdapter=new BtnPlusAdapter(presenter,0);
        autoPlusLayout.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false));
        autoPlusLayout.setAdapter(btnPlusAdapter);
        btnPlusAdapter.setDataList(nowPath,listLinkedHashMap);
        allRecycleView.setVisibility(View.VISIBLE);
        adapter=new AllFragmentAdapter();

        int size=10;
        if (list.size()<10){
            size=list.size();
        }
        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(size)
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(size)
                .build();


        LiveData<PagedList<AllFileModel>> liveData =
                new LivePagedListBuilder(new AllDataSourceFactory(context,list), config)
                        .build();
        liveData.observe(this, dataBeans -> adapter.submitList(dataBeans));
        adapter.setPresenter(presenter);
        allRecycleView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        allRecycleView.setAdapter(adapter);
       // adapter.setDataList(list);
        listLinkedHashMap.put(nowPath,list);
//        dataPasser.onDataPass(listLinkedHashMap);
        path=nowPath;
        dataPasser.onDataPass(path);
    }

    @Override
    public void updateLinkedMap(LinkedHashMap<String, List<AllFileModel>> linkedHashMap) {
        this.listLinkedHashMap=linkedHashMap;
        //dataPasser.onDataPass(listLinkedHashMap);
    }

    @Override
    public void openIntent(File f) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType( FileProvider.getUriForFile(getContext(), getActivity().getApplicationContext().getPackageName() + ".provider", f), MimeTypeMap.getSingleton().getMimeTypeFromExtension(getFileExtension(f)));
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(i);
    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "directory"; // empty extension
        }
        return name.substring(lastIndexOf+1);
    }


    @Override
    public void onEndPicture() {
        presenter.getNowPathMoreData(path);
    }

    @Override
    public boolean onBackPressed() {

        if (listLinkedHashMap.size()>1){
            Log.d("getLastKey", "onBackPressed: "+getLastKey(listLinkedHashMap));
            presenter.getNowPathMoreData(getLastKey(listLinkedHashMap));
            return true;
        }
        else {
            showToast(R.string.isLastKey);
            return false;
        }

    }
}
