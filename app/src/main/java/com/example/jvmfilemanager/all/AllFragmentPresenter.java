package com.example.jvmfilemanager.all;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

import com.example.jvmfilemanager.R;
import com.example.jvmfilemanager.base.BasePresenter;
import com.example.jvmfilemanager.base.Intention;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AllFragmentPresenter <V extends  AllFragmentContract.View> extends BasePresenter<V> implements AllFragmentContract.Presenter<V> {

    @Override
    public void getNowPathMoreData(String path) {
        getView().showProgressDialog(R.string.loading);
        File file=new File(path);
//        File[]   files = new File("/storage/emulated/0").listFiles();
        File[] files=file.listFiles();
        List<AllFileModel> fileList=new ArrayList<>();
        try {
            if (files!=null && files.length>0){
                for (File nowFile : files){
                    AllFileModel allFileModel=new AllFileModel();
                    allFileModel.setFilePath(nowFile.getCanonicalPath());
                    allFileModel.setFileName(nowFile.getName());
                    allFileModel.setExtension(getFileExtension(nowFile));
                    allFileModel.setLastModifyDate(nowFile.lastModified());
                    allFileModel.setFileLength(nowFile.length());
                    allFileModel.setFileGBMB(formetFileSizeToGBMB(allFileModel.getFileLength()));
                    allFileModel.setFileFromPath(path);
                    if (nowFile.isDirectory()) {
                        allFileModel.setDirectly(true);
                        allFileModel.setFileSize(getFilelistSize(nowFile));
                        allFileModel.setFileLength(getFileSize(nowFile));
                        allFileModel.setFileGBMB(formetFileSizeToGBMB(allFileModel.getFileLength()));
                    }
                    fileList.add(allFileModel);
                }
                Log.d("22222", "getNowPathMoreData: "+fileList);

                getView().dismissProgressDialog();
                getView().getDataFromPath(sortFileByExtension(fileList),path);
            }else if ( fileList.size()==0){
                getView().dismissProgressDialog();
                switch (getFileExtension(file)){
                    case "directory":{
                        getView().getNoData(path);
                    }

                    default:
                        getView().openIntent(file);
//                        getView().showToast(R.string.fileNullOrfilelistNull);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
            getView().showToast(R.string.IOEexception);
            getView().dismissProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //取得資料夾大小
    private long getFileSize(File f)throws Exception {
        long size = 0;
        File flist[] = f.listFiles();
        for (int i = 0; i < flist.length; i++)
        {
            if (flist[i].isDirectory())
            {
                size = size + getFileSize(flist[i]);
            } else
            {
                size = size + flist[i].length();
            }
        }
        return size;
    }

    //轉換檔案大小為GB/MB
    public String formetFileSizeToGBMB(long fileS) {//轉換檔案大小
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "K";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "G";
        }
        return fileSizeString;
    }

    //取得檔案數量
    private int getFilelistSize(File f){//遞迴求取目錄檔案個數
        int size = 0;
        File flist[] = f.listFiles();
        size=flist.length;
        for (int i = 0; i < flist.length; i++) {
            if (flist[i].isDirectory()) {
                size = size + getFilelistSize(flist[i]);
                size--;
            }
        }
        return size; }


    /**
          * 排序:大小 / 更改日期 / 檔名
        * */

    //大小遞減
    private List<AllFileModel> sortFileByLength(List<AllFileModel> list){
        Collections.sort(list, new Comparator<AllFileModel>() {

            @Override
            public int compare(AllFileModel o1, AllFileModel o2) {
                if (o1.getFileName().equals("string.txt")) Log.d("aaaaaa","DCIM"+o1.getFileLength());
                long diff = o1.getFileLength() - o2.getFileLength();
                if (diff > 0)
                    return -1;
                else if (diff == 0)
                    return 0;
                else
                    return 1;
            }

            public boolean equals(Object obj) {
                return true;
            }
        });
        return list;
    }

    private List<AllFileModel> sortFileByExtension(List<AllFileModel> list){
        Collections.sort(list, new Comparator<AllFileModel>() {
            @Override
            public int compare(AllFileModel o1, AllFileModel o2) {
                if (o1.getExtension().equals("directory") && o2.getExtension().equals("directory")){
                    return o1.getFileName().toUpperCase().compareTo(o2.getFileName().toUpperCase());
                }
                if (o1.getExtension().equals("directory") && !o2.getExtension().equals("directory"))
                    return -1;
                if (!o1.getExtension().equals("directory") && o2.getExtension().equals("directory"))
                    return 1;
                return o1.getExtension().compareTo(o2.getExtension());
            }
        });
        return list;
    }

    private List<AllFileModel> sortFileByLastModifyDate(List<AllFileModel> list){
        Collections.sort(list, new Comparator<AllFileModel>() {
            @Override
            public int compare(AllFileModel o1, AllFileModel o2) {
                long diff = o1.getLastModifyDate() - o2.getLastModifyDate();
                if (diff > 0)
                    return 1;
                else if (diff == 0)
                    return 0;
                else
                    return -1;//如果 if 中修改为 返回-1 同时此处修改为返回 1  排序就会是递减
            }

            public boolean equals(Object obj) {
                return true;
            }

        });
        return list;
    }



    @Override
    public void getClickMoreData(String path, LinkedHashMap<String, List<AllFileModel>> linkedHashMap) {
        getView().showProgressDialog(R.string.loading);
        File file=new File(path);
        File[] files=file.listFiles();
        List<AllFileModel> fileList=new ArrayList<>();
        try {
            if (files!=null){
                for (File nowFile : files){
                    AllFileModel allFileModel=new AllFileModel();
                    allFileModel.setFilePath(nowFile.getCanonicalPath());
                    allFileModel.setFileName(nowFile.getName());
                    allFileModel.setExtension(getFileExtension(nowFile));
                    if (nowFile.isDirectory()) allFileModel.setDirectly(true);
                    fileList.add(allFileModel);
                }
                Log.d("22222", "getNowPathMoreData: "+fileList);
                getView().dismissProgressDialog();
                getView().updateLinkedMap(linkedHashMap);
                getView().getDataFromPath(fileList,path);

            }else if ( fileList.size()==0){
                getView().showToast(R.string.fileNullOrfilelistNull);
                getView().dismissProgressDialog();
            }
        } catch (IOException e) {
            e.printStackTrace();
            getView().showToast(R.string.IOEexception);
            getView().dismissProgressDialog();
        }

    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "directory"; // empty extension
        }
        return name.substring(lastIndexOf+1);
    }

    // added: 20/10/22
    private FragmentManager fragmentManager;

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }
}
