package com.example.jvmfilemanager.all;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PositionalDataSource;

import com.example.jvmfilemanager.images.Images;

import java.util.ArrayList;
import java.util.List;

public class AllDataSource extends PositionalDataSource<AllFileModel> {
    private int size=10;
    private List<AllFileModel> list;
    public AllDataSource(List<AllFileModel> list) {
        this.list=list;
        if (list.size()<10){size=list.size();}
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<AllFileModel> callback) {

//        if (list.size()>10){
//            size=list.size();
//        }
        callback.onResult(loadData(0, size),0,size);
    }



    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<AllFileModel> callback) {
        Log.d("loadRange", "loadRange: "+params.startPosition);
        callback.onResult(loadData(params.startPosition, size));
    }

    private List<AllFileModel> loadData(int startPosition, int count) {
        return getIncreaseItems(startPosition,count,list);
    }

    private List<AllFileModel> getIncreaseItems(int startPosition, int count, List<AllFileModel> list) {
        List<AllFileModel> numberList=new ArrayList<>();
        int toWhich=startPosition+count;
        if (startPosition+count>list.size()){
            toWhich=list.size();
        }
        for (int i=startPosition;i<toWhich;i++){
            Log.d("loadRange", "listsize: "+list.size()+"  start: "+toWhich+"    size: "+count+
                    "    i: "+i);
//            if (i==list.size()-1)break;
            numberList.add(list.get(i));

        }
        return numberList;
    }
}
