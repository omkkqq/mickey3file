package com.example.jvmfilemanager.base;

public interface BaseAttachImpl<V extends BaseContract> {
    void onAttached(V view);
    void  onDetached();
}