package com.example.jvmfilemanager.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity implements BaseContract {

    private ProgressDialog progressDialog;
    SettingPreferenceProvider settingPreference;
    @Override
    public void init() {

    }
    @Override
    public String getResourceString(@StringRes int text) {
        return getResources().getString(text);
    }
    @Override
    public void showProgressDialog(@StringRes int s) {
        dismissProgressDialog();
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(s));
        /*dialog彈出後會點擊螢幕或物理返回鍵，dialog不消失*/
        progressDialog.setCancelable(false);
        /*dialog彈出後會點擊螢幕，dialog不消失；點擊物理返回鍵dialog消失*/
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog !=null && progressDialog.isShowing()){
            progressDialog.cancel();
        }

    }



    @Override
    public void showToast(@StringRes int text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


    public  void adjustFontScale(Configuration configuration, float scale,Context context) throws Settings.SettingNotFoundException {
        configuration.fontScale = scale;
        float systemScale = android.provider.Settings.System.getFloat(context.getContentResolver(),
                android.provider.Settings.System.FONT_SCALE);
       // float systemScale = getResources().getConfiguration().fontScale;
        configuration.fontScale = scale * systemScale;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        context.getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {

            settingPreference=new SettingPreference(this);
            if (settingPreference.getFontSize()!=null){
                adjustFontScale(this.getResources().getConfiguration(),settingPreference.getFontSize(),this);
                Log.d("eee","onStart"+settingPreference.getFontSize());
            }
            else {
                adjustFontScale(this.getResources().getConfiguration(),0.5F,this);
            }   } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("eee","onCreate");
        try {

            settingPreference=new SettingPreference(this);
            if (settingPreference.getFontSize()!=null){
                adjustFontScale(this.getResources().getConfiguration(),settingPreference.getFontSize(),this);
                Log.d("eee","onCreate"+settingPreference.getFontSize());
            }
            else {
                adjustFontScale(this.getResources().getConfiguration(),0.5F,this);
            }   } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("eee","onRestart");
        try {

            settingPreference=new SettingPreference(this);
            if (settingPreference.getFontSize()!=null){
                adjustFontScale(this.getResources().getConfiguration(),settingPreference.getFontSize(),this);
                Log.d("eee",""+settingPreference.getFontSize());
            }
            else {
                adjustFontScale(this.getResources().getConfiguration(),0.5F,this);
            }   } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {

            settingPreference=new SettingPreference(this);
            if (settingPreference.getFontSize()!=null){
                adjustFontScale(this.getResources().getConfiguration(),settingPreference.getFontSize(),this);
                Log.d("eee","onResume"+settingPreference.getFontSize());
            }
            else {
                adjustFontScale(this.getResources().getConfiguration(),0.5F,this);
            }   } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }
}

