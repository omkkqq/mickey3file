package com.example.jvmfilemanager.base;

public class BasePresenter<V extends  BaseContract> implements BaseAttachImpl<V> {

    private  V  view;

    public V getView(){
        return  view;
    }


    @Override
    public void onAttached(V view) {
        this.view=view;
    }

    public void onDetached(){
        view=null;
    }

}

