package com.example.jvmfilemanager.base;

import android.content.Context;

public class SettingPreference extends PreferenceHelper implements SettingPreferenceProvider {
    private final String SP_FONT_SIZE = "SP_FONT_SIZE";
    public SettingPreference(Context context) {
        super(context);
    }

    @Override
    public void setFontSize(Float fontSize) {
        save(Type.FLOAT,SP_FONT_SIZE,fontSize);
    }

    @Override
    public Float getFontSize() {
        return (Float) get(Type.FLOAT, SP_FONT_SIZE);
    }

    @Override
    public String getClassName() {
        return null;
    }
}
