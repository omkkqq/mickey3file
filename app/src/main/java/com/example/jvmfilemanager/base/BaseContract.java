package com.example.jvmfilemanager.base;

import androidx.annotation.StringRes;

public interface BaseContract {

    void init();

    void showProgressDialog(@StringRes int text);

    void dismissProgressDialog();

    void showToast(@StringRes int text);

    void showToast(String text);

    String getResourceString(@StringRes int text);
}
