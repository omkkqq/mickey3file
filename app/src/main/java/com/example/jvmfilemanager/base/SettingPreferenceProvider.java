package com.example.jvmfilemanager.base;

public interface SettingPreferenceProvider {

    void  setFontSize(Float fontSize);

    Float getFontSize();
}
